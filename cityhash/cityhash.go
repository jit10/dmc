package city

import (
	"encoding/binary"
	"hash"
)

type (
	sum32 uint32
)

const (
	c1 = 0xcc9e2d51
	c2 = 0x1b873593
)

func bswap32(x uint32) uint32 {
	return uint32(uint32(x>>24) + uint32((x&0x00ff0000)>>8) + uint32((x&0x0000ff00)<<8) + uint32(x<<24))
}

func rot32(in uint32, shift uint8) uint32 {
	if shift == 0 {
		return in
	} else {
		return in>>shift | in<<(32-shift)
	}
}

func mur(x, y uint32) uint32 {
	x = c2 * rot32(x*c1, 17)
	y = rot32(x^y, 19)
	return y*5 + 0xe6546b64
}

func fmix(x uint32) uint32 {
	x ^= x >> 16
	x *= 0x85ebca6b
	x ^= x >> 13
	x *= 0xc2b2ae35
	x ^= x >> 16
	return x
}

func hash32Len13to24(ba []byte) uint32 {
	l := uint32(len(ba))
	a := binary.LittleEndian.Uint32(ba[l/2-4:])
	b := binary.LittleEndian.Uint32(ba[4:])
	c := binary.LittleEndian.Uint32(ba[l-8:])
	d := binary.LittleEndian.Uint32(ba[l/2:])
	e := binary.LittleEndian.Uint32(ba)
	f := binary.LittleEndian.Uint32(ba[l-4:])
	return fmix(mur(f, mur(e, mur(d, mur(c, mur(b, mur(a, l)))))))
}

func hash32Len0to4(ba []byte) uint32 {
	var b = uint32(0)
	var c = uint32(9)
	for _, x := range ba {
		b = b*c1 + uint32(int8(x))
		c ^= b
	}
	return fmix(mur(b, mur(uint32(len(ba)), c)))
}

func hash32Len5to12(ba []byte) uint32 {
	l := uint32(len(ba))
	a := uint32(l)
	b := l * 5
	c := uint32(9)
	d := b
	a += binary.LittleEndian.Uint32(ba)
	b += binary.LittleEndian.Uint32(ba[l-4:])
	c += binary.LittleEndian.Uint32(ba[l/2&4:])
	return fmix(mur(c, mur(b, mur(a, d))))
}

func New32() hash.Hash32 {
	var s sum32 = 0
	return &s
}

func (s *sum32) Reset() { *s = 0 }

func (s *sum32) Sum32() uint32 { return uint32(*s) }

func (s *sum32) Write(d []byte) (int, error) {
	l := uint32(len(d))
	if l <= 24 {
		if l <= 12 {
			if l <= 4 {
				*s = sum32(hash32Len0to4(d))
			} else {
				*s = sum32(hash32Len5to12(d))
			}
		} else {
			*s = sum32(hash32Len13to24(d))
		}
		return len(d), nil
	}

	h := l
	g := c1 * l
	f := g
	a0 := rot32(binary.LittleEndian.Uint32(d[l-4:])*c1, 17) * c2
	a1 := rot32(binary.LittleEndian.Uint32(d[l-8:])*c1, 17) * c2
	a2 := rot32(binary.LittleEndian.Uint32(d[l-16:])*c1, 17) * c2
	a3 := rot32(binary.LittleEndian.Uint32(d[l-12:])*c1, 17) * c2
	a4 := rot32(binary.LittleEndian.Uint32(d[l-20:])*c1, 17) * c2
	h = rot32(a0^h, 19)
	h = h*5 + 0xe6546b64
	h = rot32(a2^h, 19)
	h = h*5 + 0xe6546b64
	g = rot32(a1^g, 19)
	g = g*5 + 0xe6546b64
	g = rot32(a3^g, 19)
	g = g*5 + 0xe6546b64
	f = rot32(a4+f, 19)
	f = f*5 + 0xe6546b64

	b := d
	for i := uint32(0); i < (l-1)/20; i++ {
		a0 = rot32(binary.LittleEndian.Uint32(b)*c1, 17) * c2
		a1 = binary.LittleEndian.Uint32(b[4:])
		a2 = rot32(binary.LittleEndian.Uint32(b[8:])*c1, 17) * c2
		a3 = rot32(binary.LittleEndian.Uint32(b[12:])*c1, 17) * c2
		a4 = binary.LittleEndian.Uint32(b[16:])
		h = rot32(a0^h, 18)
		h = h*5 + 0xe6546b64
		f = c1 * rot32(a1+f, 19)
		g = rot32(a2+g, 18)
		g = g*5 + 0xe6546b64
		h = rot32(h^(a3+a1), 19)
		h = h*5 + 0xe6546b64
		g = bswap32(a4^g) * 5
		h = bswap32(h + a4*5)
		f += a0
		f, h = h, f
		f, g = g, f //PERMUTE3(f, h, g)
		b = b[20:]
	}
	g = rot32(g, 11) * c1
	g = rot32(g, 17) * c1
	f = rot32(f, 11) * c1
	f = rot32(f, 17) * c1
	h = rot32(h+g, 19)
	h = h*5 + 0xe6546b64
	h = rot32(h, 17) * c1
	h = rot32(h+f, 19)
	h = h*5 + 0xe6546b64
	h = rot32(h, 17) * c1
	*s = sum32(h)
	return len(d), nil
}

func (s *sum32) Size() int { return 4 }

func (s *sum32) BlockSize() int { return 1 }

func (s *sum32) Sum(in []byte) []byte {
	v := uint32(*s)
	return append(in, byte(v>>24), byte(v>>16), byte(v>>8), byte(v))
}

func Hash32(in []byte) uint32 {
	var s sum32
	(&s).Write(in)
	return uint32(s)
}
