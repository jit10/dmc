## Install

1. Go toolchain(>= 1.3). [`$GOPATH`](https://golang.org/doc/code.html#GOPATH) should be set properly.
2. protoc. For ubuntu.`aptitude install protobuf-compiler`.
3. Download. `go get -d bitbucket.org/jpathy/dmc`.
4. Install gogoprotobuf. add `$GOPATH/bin` to `$PATH`. `cd $GOPATH/src/code.google.com/p/gogoprotobuf; make`.
5. compile. `cd $GOPATH/src/bitbucket.org/jpathy/dmc; make`.

## Run
1. ndbserver. (To query and keep track of dbservers). `./ndbserver -config ndb.xml`.
2. Launch as many instances of dbserver as required. `./dbserver -config testing/config#.xml // Replace # with instance no.`

## Client API
1. For clients written in Go refer (https://godoc.org/bitbucket.org/jpathy/dmc/db#Client).
2. The json http API is below.

		All requests can be sent to any server which then redirects to the leader. 
		Clients must follow redirects. For performance reasons the leader location can be cached.
	
		json Requests are of the form. {"Type": T, "Request": R}
	
		Configuration Changes must be submitted as http POST to url http://host:port/config.
		For Configuration
		T, R :=
				| "Add", {"ServerId": ID}
				| "Del", {"ServerId": ID}
				| "Reconfigure", {"Servers": IDs}
		and ID :> JSON string and IDs :> JSON array of strings.
		http response status code 200. indicate a success otherwise failure.
	
		Store Requests must be submitted as http POST to url http://host:port/store.
		For storage
		T, R := 
				| "Read", {"Timestamp": t, "Key": k}
				| "Write", {"Key": k, "Value": v}
		Note: for latest reads t can be set to -1.
	
		Results are of the form {"Type": T, "Result": R, "Error": E}.
		E = "" for success else failure.
		T, R :=
				| "Read",  {"Timestamp": t, "Value": v}
				| "Write", {"TimeStamp": t}
		
		E :> JSON string t :> JSON number k, v :> base64-encoded JSON string.
