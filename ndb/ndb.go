package ndb

import (
	"bufio"
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net"
	"os"
	"sync"
)

const SrvPort = "31415"

const (
	AnnTyp   = "Announce"
	QueryTyp = "Query"
)

const (
	AnnOK = iota
	QueryOK
	QueryEmpty
)

var (
	ErrCert     = fmt.Errorf("invalid Cert")
	ErrAnnounce = fmt.Errorf("failed to Announce")
	ErrQuery    = fmt.Errorf("query Result Empty")
)

type Config struct {
	NSHost   string `xml:"NDB>Hostname"`
	NSSecret string `xml:"NDB>SecretKey"`
}

type Ann struct {
	Uid      string
	SMPort   uint
	RaftPort uint
}

type SMInfo struct {
	BootLeader bool
	Ann
	Host string
}

type Query struct {
	Uid string
}

type Request struct {
	Secret []byte
	Typ    string          `json:"Type"`
	Msg    json.RawMessage `json:"Request"`
}

type Response struct {
	Code int
	Msg  json.RawMessage `json:"Response"`
}

type Server struct {
	sync.Mutex
	sync.WaitGroup
	ln     net.Listener
	secret []byte
	stats  map[string]*SMInfo
}

func ParseConfig(confPath string) (conf *Config, err error) {
	f, err := os.Open(confPath)
	conf = new(Config)
	if err == nil {
		defer f.Close()
		err = xml.NewDecoder(f).Decode(conf)
	}
	if err != nil {
		*conf = Config{}
	}
	return
}

func NewServer(secret []byte) (s *Server, err error) {
	ln, err := net.Listen("tcp", ":"+SrvPort)
	if err != nil {
		return
	}

	s = &Server{
		ln:     ln,
		secret: secret,
		stats:  make(map[string]*SMInfo),
	}

	return
}

func (s *Server) Close() {
	s.ln.Close()
	s.Wait()
}

func (s *Server) Serve() (err error) {
	for {
		c, err := s.ln.Accept()
		if err != nil {
			break
		}

		s.Add(1)
		go func(tc net.Conn) {
			defer s.Done()

			wr := bufio.NewWriter(tc)

			var err error
			var req Request

			dec := json.NewDecoder(tc)
			if err = dec.Decode(&req); err != nil || bytes.Compare(req.Secret, s.secret) != 0 {
				tc.Close()
				return
			}

			switch req.Typ {
			case AnnTyp:
				var info SMInfo
				h, _, _ := net.SplitHostPort(c.RemoteAddr().String())

				if err = json.Unmarshal(req.Msg, &info.Ann); err != nil {
					break
				}
				info.Host = h

				s.Lock()
				if len(s.stats) == 0 {
					info.BootLeader = true
				}

				if sinfo, ok := s.stats[info.Uid]; ok {
					sinfo.Host = info.Host
					sinfo.Ann = info.Ann
				} else {
					s.stats[info.Uid] = &info
				}
				s.Unlock()

				enc := json.NewEncoder(wr)
				enc.Encode(Response{Code: AnnOK})
				wr.Flush()

			case QueryTyp:
				var resp Response
				var q Query

				if err = json.Unmarshal(req.Msg, &q); err != nil {
					break
				}

				s.Lock()
				infos := make([]*SMInfo, 0, len(s.stats))
				switch q.Uid {
				case "":
					for _, i := range s.stats {
						infos = append(infos, i)
					}

				default:
					if smi, ok := s.stats[q.Uid]; ok {
						infos = append(infos, smi)
					}
				}
				s.Unlock()

				if len(infos) == 0 {
					resp.Code = QueryEmpty
				} else {
					if resp.Msg, err = json.Marshal(infos); err != nil {
						break
					}
					resp.Code = QueryOK
				}

				enc := json.NewEncoder(wr)
				enc.Encode(&resp)
				wr.Flush()

			default:
			}

			tc.Close()
		}(c)
	}

	return
}

type Client struct {
	secret  []byte
	srvAddr string
}

func NewClient(conf *Config) (c *Client) {
	c = &Client{
		secret:  []byte(conf.NSSecret),
		srvAddr: conf.NSHost + ":" + SrvPort,
	}

	return
}

func (c *Client) request(req *Request) (resp *Response, err error) {
	tc, err := net.Dial("tcp", c.srvAddr)
	if err != nil {
		return
	}

	enc := json.NewEncoder(tc)

	if err = enc.Encode(req); err != nil {
		return
	}

	resp = new(Response)
	dec := json.NewDecoder(tc)
	err = dec.Decode(&resp)

	tc.Close()
	return
}

func (c *Client) Announce(uid string, smport, raftport uint) (err error) {
	var msg json.RawMessage
	if msg, err = json.Marshal(Ann{
		Uid:      uid,
		SMPort:   smport,
		RaftPort: raftport,
	}); err != nil {
		return
	}

	resp, err := c.request(&Request{
		Secret: c.secret,
		Typ:    AnnTyp,
		Msg:    msg,
	})
	if err != nil {
		if err == io.EOF {
			return
		}
		return
	}

	if resp.Code != AnnOK {
		err = ErrAnnounce
	}

	return
}

func (c *Client) Query(uid string) (sms []*SMInfo, err error) {
	var msg json.RawMessage
	if msg, err = json.Marshal(Query{
		Uid: uid,
	}); err != nil {
		return
	}

	resp, err := c.request(&Request{
		Secret: c.secret,
		Typ:    QueryTyp,
		Msg:    msg,
	})
	if err != nil {
		if err == io.EOF {
			err = ErrQuery
		}
		return
	}

	if resp.Code != QueryOK {
		err = ErrQuery
		return
	}

	err = json.Unmarshal(resp.Msg, &sms)
	return
}
