package util

import (
	"os"
	"syscall"
)

const (
	MMAP_RD   = (1 << 0)
	MMAP_WR   = (1 << 1)
	MMAP_EXEC = (1 << 2)
)

func MmapFile(f *os.File, prot int, off, size int64) (b []byte, err error) {
	var lprot int
	if prot&MMAP_RD == MMAP_RD {
		lprot |= syscall.PROT_READ
	}
	if prot&MMAP_WR == MMAP_WR {
		lprot |= syscall.PROT_WRITE
	}
	b, err = syscall.Mmap(int(f.Fd()), off, int(size), lprot, syscall.MAP_SHARED)
	return
}
