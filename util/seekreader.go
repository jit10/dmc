package util

import (
	"io"
)

type SeekReader struct {
	io.ReaderAt
	Off int64
}

func (r SeekReader) Read(p []byte) (int, error) {
	return r.ReadAt(p, r.Off)
}

// copied from pkg os.
// Basename removes trailing slashes and the leading directory name from path name
func Basename(name string) string {
	i := len(name) - 1
	// Remove trailing slashes
	for ; i > 0 && name[i] == '/'; i-- {
		name = name[:i]
	}
	// Remove leading directory name
	for i--; i >= 0; i-- {
		if name[i] == '/' {
			name = name[i+1:]
			break
		}
	}

	return name
}

