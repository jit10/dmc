package util

import (
	"io"
)

type ByteBuffer []byte

func (buf ByteBuffer) Write(p []byte) (n int, err error) {
	n = copy(buf, p)
	if n < len(p) {
		err = io.EOF
	}
	return
}

func (buf ByteBuffer) Read(p []byte) (n int, err error) {
	n = copy(p, buf)
	if n < len(p) {
		err = io.EOF
	}
	return n, err
}
