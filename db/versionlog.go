package db

import (
	"errors"
	"io"
	"os"

	. "bitbucket.org/jpathy/dmc/util"
)

const indexSize = 8

var (
	EPut = errors.New("Immutable store doesn't implement Put method")
)

type index int64

func (t *index) Unmarshal(r io.Reader) error {
	var b [8]byte
	bs := b[:8]
	if _, err := io.ReadAtLeast(r, bs, 8); err != nil {
		return err
	}
	*t = index((uint64(bs[0]) | (uint64(bs[1]) << 8) | (uint64(bs[2]) << 16) | (uint64(bs[3]) << 24) | (uint64(bs[4]) << 32) | (uint64(bs[5]) << 40) | (uint64(bs[6]) << 48) | (uint64(bs[7]) << 56)))
	return nil
}

type versionLog struct {
	posOfftbl  int64
	sizeOfftbl int64
	*os.File
}

/* Satisfy cuckoo.PositionTable */

func (vl *versionLog) GetPosAt(idx int64) (pos int64) {
	var versidx index
	if err := versidx.Unmarshal(SeekReader{vl.File, vl.posOfftbl + int64(idx*indexSize)}); err != nil {
		panic(err)
	}
	pos = int64(versidx)
	return
}

func (vl *versionLog) PutPosAt(idx, pos int64) (err error) {
	return EPut
}

/* ... */

func (vl *versionLog) readHeader(hdr *versionHeader, epos int64) error {
	err := hdr.Unmarshal(SeekReader{vl, epos})
	return err
}

func (vl *versionLog) readKey(k []byte, kpos int64) error {
	_, err := vl.ReadAt(k, kpos)
	return err
}

func (vl *versionLog) readVersions(vers []version, vpos int64) error {
	var err error
	for i := range vers {
		if err = vers[i].Unmarshal(SeekReader{vl, vpos}); err != nil {
			break
		}
		vpos += versionSize
	}
	return err
}

func (vl *versionLog) lastVersion(ver *version, epos int64) error {
	var hdr versionHeader
	err := vl.readHeader(&hdr, epos)
	if err != nil {
		return err
	}
	if hdr.nvers == 0 {
		panic(errors.New("No of versions can't be 0 in ts.log"))
	}
	err = ver.Unmarshal(SeekReader{vl, epos + versionHeaderSize + int64(hdr.klen)})
	return err
}

func openVersionLog(path string) (vl versionLog, err error) {
	f, err := os.OpenFile(path+string(os.PathSeparator)+TSLogName, os.O_CREATE|os.O_RDONLY, 0600)
	if err != nil {
		return
	}

	st, err := f.Stat()
	if err != nil {
		return
	}

	var ftr indexTableFooter
	end := st.Size() - indexTableFooterSize
	if err = ftr.Unmarshal(SeekReader{f, end}); err != nil {
		return
	}

	vl = versionLog{posOfftbl: ftr.pos, sizeOfftbl: (end - ftr.pos) / indexSize, File: f}
	return
}
