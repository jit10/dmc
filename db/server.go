package db

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/jpathy/dmc/ndb"
	"bitbucket.org/jpathy/dmc/truetime"
	"bitbucket.org/jpathy/raft"
)

const (
	MaxHttpBodySize = 10 * 1024 * 1024

	raftName = "_raft"
	tabName  = "tab"

	statusPath = "/status"
	storePath  = "/store"
	configPath = "/config"
)

const (
	wrCmdID = iota + raft.ReservedIDs + 1
	tsCmdID
)

var (
	ErrTemp  = "Temporary Failure"
	BadReq   = fmt.Errorf("invalid request type")
	BadWrReq = fmt.Errorf("vnvalid Write Request")
)

var defaultParams = raft.ConfigParams{
	ServerID:     "",
	BatchLimit:   100,
	MaxReqSize:   64 * 1024,
	MaxEntries:   8 * 1024,
	BaseTimeUnit: 1000, // 1ms
	ElectTmUnits: 1000,
}

type serverRequest struct {
	Typ string          `json:"Type"`
	Msg json.RawMessage `json:"Request"`
}

type serverResult struct {
	Typ string `json:"Type"`
	Res Result `json:"Result"`
	Err string `json:"Error"`
}

type WriteResult struct {
	T int64 `json:"TimeStamp"`
}

func (wr WriteResult) Error() error {
	return nil
}

// ID() Implements raft.Command for WriteRequest.
func (wr *WriteRequest) ID() uint8 {
	return wrCmdID
}

// ID() Implements raft.Command for ReadTS.
func (ts *ReadTS) ID() uint8 {
	return tsCmdID
}

func id2Addr(ns *ndb.Client, uid string) (host string, smport uint, raftport uint) {
	infos, err := ns.Query(uid)
	if err != nil || len(infos) != 1 {
		return
	}

	return infos[0].Host, infos[0].SMPort, infos[0].RaftPort
}

func findLeaderAddr(ns *ndb.Client, self string) (addr string, err error) {
	var infos []*ndb.SMInfo
	infos, err = ns.Query("")
	if err != nil || len(infos) == 0 {
		err = ndb.ErrQuery
		return
	}

	for _, info := range infos {
		if info.BootLeader && info.Uid != self {
			addr = info.Host + ":" + strconv.Itoa(int(info.SMPort))
			return
		}
	}

	return
}

func decodeSrvRequest(r io.Reader) (sreq []serverRequest, err error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return
	}

	err = json.Unmarshal(b, &sreq)
	return
}

type stats struct {
	procTm  time.Duration
	raftTm  time.Duration
	execTm  time.Duration
	storeTm time.Duration
}

func (st stats) Show() {
	var raftTm time.Duration
	if st.raftTm > st.execTm {
		raftTm = st.raftTm - st.execTm
	}

	fmt.Println("\n--statistics--\n")
	fmt.Println("Time spent Processing:", st.procTm.Seconds())
	fmt.Println("Time spent in consensus:", raftTm.Seconds())
	fmt.Println("Time spent in commit wait:", (st.execTm - st.storeTm).Seconds())
	fmt.Println("Time spent in store:", st.storeTm.Seconds())
	fmt.Println("\n--------------\n")
}

type tabSrv struct {
	// First field in struct guranteed to be 64bit aligned for CAS.
	safeTm int64

	seqmap map[string]uint64

	qC chan bool

	rs raft.Server

	*Tablet

	stats
}

func newTabServer(name string, conf *Config) (s *tabSrv, err error) {
	tab, err := OpenTablet(name, conf)
	if err != nil {
		return
	}

	tabs := &tabSrv{
		seqmap: make(map[string]uint64),
		qC:     make(chan bool),
		Tablet: tab,
	}
	params := defaultParams
	params.ServerID = conf.Uuid
	params.LogPath = conf.DBPath + string(os.PathSeparator) + name + string(os.PathSeparator) + raftName
	rs, err := raft.NewServer(&params, tabs, commFunc(conf), nil)
	if err != nil {
		return nil, err
	}
	tabs.rs = rs

	s = tabs
	return
}

func (tabs *tabSrv) Close() error {
	select {
	case <-tabs.qC:
		return nil
	default:
		close(tabs.qC)
	}
	tabs.rs.Stop()
	return tabs.Tablet.Close()
}

// exec() could be called in multiple goroutines.
func (tabs *tabSrv) exec(req Request) (res Result) {
	start := time.Now()
	defer func() {
		atomic.AddInt64((*int64)(&tabs.stats.storeTm), int64(time.Now().Sub(start)))
	}()

	resC := tabs.Tablet.Do(req)
	res = <-resC

	return
}

func (tabs *tabSrv) raftIssue(clcmd *raft.ClientCommand) (ldrId string, err error) {
	start := time.Now()
	defer func() {
		atomic.AddInt64((*int64)(&tabs.stats.raftTm), int64(time.Now().Sub(start)))
	}()

	var clrespC <-chan *raft.ClientResponse
	for i := 0; i < 100; i++ {
		clrespC, err = tabs.rs.Issue(clcmd)
		if err != nil {
			if err == raft.ErrCmdCap {
				time.Sleep(100 * time.Millisecond)
				continue
			} else {
				return
			}
		}

		clresp := <-clrespC
		if err = clresp.Error(); err != nil && err == raft.ErrNotLeader {
			ldrId = clresp.LeaderID()
		}
		break
	}
	return
}

// Execute implements raft.StateMachine.
func (tabs *tabSrv) Execute(clcmd *raft.ClientCommand) (resp raft.Response) {
	start := time.Now()
	defer func() {
		atomic.AddInt64((*int64)(&tabs.stats.execTm), int64(time.Now().Sub(start)))
	}()

	cmd := clcmd.Command()
	switch cmd.(type) {
	case *ReadTS:
		resp = ErrVal{nil}

	case *WriteRequest:
		wrq, ok := cmd.(*WriteRequest)
		if !ok {
			resp = ErrVal{BadWrReq}
		}

		if seq, ok := tabs.seqmap[clcmd.ClientID()]; ok && seq >= clcmd.Sequence() {
			resp = ErrVal{nil}
			return
		}

		tabs.seqmap[clcmd.ClientID()] = clcmd.Sequence()

		// commit wait.
		TT.Until(wrq.T)

		atomic.StoreInt64(&tabs.safeTm, wrq.T)
		resp = tabs.exec(wrq)

	default:
		resp = ErrVal{BadWrReq}
	}

	return
}

// Snapshot implements raft.StateMachine.
func (tabs *tabSrv) Snapshot(w io.Writer) error {
	return tabs.Tablet.Snapshot(w)
}

// Restore implements raft.StateMachine.
func (tabs *tabSrv) Restore(r io.Reader) (err error) {
	return tabs.Tablet.Restore(r)
}

// A DB Server Instance.
type Server interface {
	Serve() error
	Close() error
}

type server struct {
	// fields to shutdown http server properly.
	sync.Mutex
	cwg   sync.WaitGroup
	quitC chan bool
	conns map[string]net.Conn

	uid string
	seq uint64

	ns *ndb.Client

	ln  net.Listener
	srv *http.Server

	tabs *tabSrv
}

func NewServer(confFile string) (s Server, err error) {
	conf, err := ParseDBConfig(confFile)
	if err != nil {
		return
	}

	ns := ndb.NewClient(&conf.Config)

	if err = ns.Announce(conf.Uuid, uint(conf.Port), uint(conf.Port)+1); err != nil {
		return
	}

	dbs := &server{
		uid:   conf.Uuid,
		quitC: make(chan bool),
		conns: make(map[string]net.Conn),
	}

	TT.InitGPS()
	raft.RegisterCommands(&ReadTS{}, &WriteRequest{})

	if dbs.tabs, err = newTabServer(tabName, conf); err != nil {
		return
	}

	mux := http.NewServeMux()
	mux.HandleFunc(statusPath, dbs.statusHandler())
	mux.HandleFunc(storePath, dbs.storeHandler())
	mux.HandleFunc(configPath, dbs.confHandler())

	dbs.ns = ns
	dbs.srv = &http.Server{
		Addr:      ":" + strconv.Itoa(int(conf.Port)),
		Handler:   mux,
		ConnState: dbs.connHandler,
	}

	s = dbs
	return
}

func (dbs *server) Serve() (err error) {
	// Set the no. of execution contexts to 4.
	runtime.GOMAXPROCS(4)

	errC := make(chan error, 1)

	srv := dbs.srv
	ln, err := net.Listen("tcp", srv.Addr)
	if err != nil {
		return
	}
	dbs.ln = ln

	go func() {
		errC <- srv.Serve(ln)
	}()

	var ldr string
	bstrap := dbs.tabs.rs.CanBootstrap()
	if bstrap {
		if ldr, err = findLeaderAddr(dbs.ns, dbs.uid); err != nil {
			return
		}

		if ldr == "" {
			if err = dbs.tabs.rs.Bootstrap(); err != nil {
				return
			}
		}
	}
	if err = dbs.tabs.rs.Start(); err != nil {
		return
	}

	// Join leader for first time.
	if bstrap && ldr != "" {
		c := &Client{
			ns:     dbs.ns,
			ldrUrl: "http://" + ldr,
			htc: &http.Client{
				Timeout: 5 * time.Minute,
			},
		}
		if err = c.Add(dbs.uid); err != nil {
			time.Sleep(5 * time.Second)
			return
		}
	}

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)

	select {
	case err = <-errC:

	case <-c:
		err = dbs.Close()
	}

	return
}

func (dbs *server) Close() (err error) {
	dbs.srv.SetKeepAlivesEnabled(false)
	dbs.ln.Close()

	select {
	case <-dbs.quitC:
		return

	default:
		close(dbs.quitC)
	}

	dbs.Lock()
	for s, c := range dbs.conns {
		c.Close()
		delete(dbs.conns, s)
	}
	dbs.Unlock()

	dbs.cwg.Wait()

	dbs.tabs.stats.Show()

	return dbs.tabs.Close()
}

func (dbs *server) statusHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
	}
}

func (dbs *server) connHandler(c net.Conn, st http.ConnState) {
	switch st {
	case http.StateNew:
		dbs.cwg.Add(1)

	case http.StateClosed, http.StateHijacked:
		dbs.cwg.Done()

	case http.StateActive:
		dbs.Lock()
		delete(dbs.conns, c.RemoteAddr().String())
		dbs.Unlock()

	case http.StateIdle:
		select {
		case <-dbs.quitC:
			c.Close()

		default:
			dbs.Lock()
			dbs.conns[c.RemoteAddr().String()] = c
			dbs.Unlock()
		}
	}
}

func (dbs *server) nextSequence() uint64 {
	return atomic.AddUint64(&dbs.seq, 1) - 1
}

func (dbs *server) confHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		tabs := dbs.tabs

		if ldrId := tabs.rs.LeaderID(); ldrId != dbs.uid {
			h, p, _ := id2Addr(dbs.ns, ldrId)
			if h != "" {
				http.Redirect(w, r, "http://"+h+":"+strconv.Itoa(int(p)), http.StatusTemporaryRedirect)
			}
			return
		}

		var sreq serverRequest
		b, err := ioutil.ReadAll(http.MaxBytesReader(w, r.Body, MaxHttpBodySize))
		if err == nil {
			err = json.Unmarshal(b, &sreq)
		}

		if err != nil {
			http.Error(w, "", http.StatusBadRequest)
			return
		}
		var cmd raft.Command
		switch sreq.Typ {
		case "Add":
			cmd = new(raft.AddServerCommand)

		case "Del":
			cmd = new(raft.DelServerCommand)

		case "Reconfigure":
			cmd = new(raft.ReconfigureCommand)

		default:
			http.Error(w, "", http.StatusBadRequest)
			return
		}

		if err = json.Unmarshal(sreq.Msg, cmd); err != nil {
			http.Error(w, "", http.StatusBadRequest)
			return
		}

		ldrId, err := tabs.raftIssue(raft.MkClientCommand(dbs.nextSequence(), dbs.uid, cmd))
		if ldrId != "" {
			h, p, _ := id2Addr(dbs.ns, ldrId)
			if h != "" {
				http.Redirect(w, r, "http://"+h+":"+strconv.Itoa(int(p)), http.StatusTemporaryRedirect)
			}
			return
		}

		// Success.
		w.WriteHeader(http.StatusOK)
	}
}

func (dbs *server) storeHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		tabs := dbs.tabs

		if ldrId := tabs.rs.LeaderID(); ldrId != dbs.uid {
			if ldrId == "" {
				ldrId = dbs.uid
			}
			h, p, _ := id2Addr(dbs.ns, ldrId)
			if h != "" {
				http.Redirect(w, r, "http://"+h+":"+strconv.Itoa(int(p)), http.StatusTemporaryRedirect)
			}
			return
		}

		var reqs []serverRequest
		b, err := ioutil.ReadAll(http.MaxBytesReader(w, r.Body, MaxHttpBodySize))
		if err == nil {
			err = json.Unmarshal(b, &reqs)
		}

		if err != nil {
			http.Error(w, "", http.StatusBadRequest)
			return
		}

		rqs := make([]Request, len(reqs))
		for i, req := range reqs {
			switch req.Typ {
			case WriteType:
				wrq := new(WriteRequest)
				if err = json.Unmarshal(req.Msg, wrq); err != nil {
					goto decodeEnd
				}
				rqs[i] = wrq

			case ReadType:
				rrq := new(ReadRequest)
				if err = json.Unmarshal(req.Msg, rrq); err != nil {
					goto decodeEnd
				}
				rqs[i] = rrq

			default:
				err = BadReq
				goto decodeEnd
			}
		}
	decodeEnd:
		if err != nil {
			http.Error(w, "", http.StatusBadRequest)
			return
		}

		var wg sync.WaitGroup
		var lck sync.Mutex
		var gldrId string

		res := make([]serverResult, len(rqs))
		wg.Add(len(rqs))

		start := time.Now()

		for i, rq := range rqs {
			switch rq.(type) {
			case *WriteRequest:
				wrq := rq.(*WriteRequest)

				// Assign Timestamp.
				wrq.T = TT.Now().Lt
				go func(i int) {
					defer wg.Done()

					ldrId, err := tabs.raftIssue(raft.MkClientCommand(dbs.nextSequence(), dbs.uid, wrq))

					res[i].Typ = "Write"
					if err != nil {
						lck.Lock()
						if ldrId != "" {
							gldrId = ldrId
						}
						lck.Unlock()

						res[i].Res = WriteResult{-1}
						res[i].Err = ErrTemp
					} else {
						res[i].Res = WriteResult{wrq.T}
					}
				}(i)

			case *ReadRequest:
				rrq := rq.(*ReadRequest)

				go func(i int) {
					defer wg.Done()

					var ldrId string
					var err error
					switch rrq.T {
					// Latest Read seen.
					case -1:
						rrq.T = atomic.LoadInt64(&tabs.safeTm)
						ldrId, err = tabs.raftIssue(raft.MkClientCommand(0, "", &ReadTS{T: rrq.T}))

					default:
						//waitUntil(tabs.safeTm >= rrq.T)
					}

					res[i].Typ = "Read"
					if err != nil {
						lck.Lock()
						if ldrId != "" {
							gldrId = ldrId
						}
						lck.Unlock()
					} else {
						res[i].Res = tabs.exec(rrq)
						err = res[i].Res.Error()
					}

					if err != nil {
						res[i].Err = err.Error()
					}
				}(i)
			}
		}

		wg.Wait()
		atomic.AddInt64((*int64)(&tabs.stats.procTm), int64(time.Now().Sub(start)))

		if gldrId != "" {
			h, p, _ := id2Addr(dbs.ns, gldrId)
			if h != "" {
				w.Header().Set("Location", "http://"+h+":"+strconv.Itoa(int(p)))
			}
		}

		if err = json.NewEncoder(w).Encode(res); err != nil {
			http.Error(w, "Response Failure:"+err.Error(), http.StatusInternalServerError)
		}
	}
}
