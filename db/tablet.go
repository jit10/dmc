package db

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"strconv"
	"sync"

	"bitbucket.org/jpathy/dmc/log"
)

const (
	ReqPipelineSize   = 8
	MaxConcurrentReqs = 16

	SName = "store"

	LFile = ".lstore"
	HFile = ".hstore"
)

const (
	stopEvent   = true
	resumeEvent = false
)

const (
	ReadType  = "Read"
	WriteType = "Write"
)

type Request interface {
	Typ() string
}

type ReadRequest struct {
	T int64  `json:"Timestamp"`
	K []byte `json:"Key"`
}

// Typ() Implements Request for ReadRequest.
func (rd *ReadRequest) Typ() string {
	return ReadType
}

// Typ() Implements Request for WriteRequest.
func (wr *WriteRequest) Typ() string {
	return WriteType
}

type Result interface {
	Error() error
}

type ReadResult struct {
	T int64  `json:"Timestamp"`
	V []byte `json:"Value"`
	ErrVal
}

type ErrVal struct {
	error
}

func (res ErrVal) Error() error {
	return res.error
}

type internalReq struct {
	req  Request
	resC chan Result
}

type Tablet struct {
	sync.Mutex
	gsync sync.WaitGroup
	psync sync.WaitGroup
	csync sync.WaitGroup
	ssync sync.WaitGroup

	state chan bool
	quit  chan bool
	limit chan struct{}

	reqQ chan internalReq
	conf *Config

	path   string
	stores []Store
}

func openStore(path string, conf *Config, create bool) (s Store, err error) {
	var fi os.FileInfo
	if fi, err = os.Stat(path); err == nil && fi.IsDir() {
		if _, err = os.Stat(path + string(os.PathSeparator) + HFile); err == nil {
			return OpenHashStore(path)
		} else if _, err = os.Stat(path + string(os.PathSeparator) + LFile); err == nil {
			return OpenLogStore(path, conf)
		} else {
			if _, err = os.Stat(path + string(os.PathSeparator) + TSLogName); err == nil {
				var f *os.File
				if f, err = os.Create(path + string(os.PathSeparator) + HFile); err == nil {
					return openStore(path, conf, create)
					defer f.Close()
				}
			} else {
				var f *os.File
				if f, err = os.Create(path + string(os.PathSeparator) + LFile); err == nil {
					return openStore(path, conf, create)
					defer f.Close()
				}
			}
			return nil, err
		}
	} else if os.IsNotExist(err) {
		if !create {
			return
		}
		if err = os.Mkdir(path, 0755); err != nil {
			return
		}
		var f *os.File
		if f, err = os.Create(path + string(os.PathSeparator) + LFile); err == nil {
			defer f.Close()
		} else {
			return
		}
		return OpenLogStore(path, conf)
	}
	return
}

func OpenTablet(name string, conf *Config) (tab *Tablet, err error) {
	var s Store
	stores := make([]Store, 0, conf.MaxHStore+1) //Change it to 2 when sorted store is implemented

	path := conf.DBPath + string(os.PathSeparator) + name + string(os.PathSeparator)
	_, err = os.Stat(path)
	if err == nil {
		for i := 0; i < cap(stores); i++ {
			sn := path + SName + strconv.Itoa(i)
			if s, err = openStore(sn, conf, false); err != nil {
				break
			}
			stores = append(stores, s)
		}
		if len(stores) >= 0 {
			err = nil
		}
	} else {
		if os.IsNotExist(err) {
			err = os.MkdirAll(path, 0755)
		}
		if err != nil {
			return
		}
		s, err = openStore(path+SName+strconv.Itoa(0), conf, true)
		if err != nil {
			return
		}
		stores = append(stores, s)
	}
	tab = &Tablet{
		conf:   conf,
		path:   path,
		stores: stores,
		quit:   make(chan bool),
		state:  make(chan bool),
		limit:  make(chan struct{}, MaxConcurrentReqs),
		reqQ:   make(chan internalReq, ReqPipelineSize),
	}
	go tab.reqHandler()
	return tab, nil
}

func (tab *Tablet) get(t int64, k []byte) (tʹ int64, v []byte, err error) {
	defer tab.gsync.Done()

	nvers := int(tab.conf.Versions)
	for i := len(tab.stores) - 1; i >= 0; i-- {
		tʹ, v, err = tab.stores[i].Get(t, k, &nvers)
		if err != nil || v != nil {
			return
		}
		if nvers == 0 {
			break
		}
	}
	return 0, nil, fmt.Errorf("key %v doesn't exist at %v", string(k), t)
}

func (tab *Tablet) put(t int64, k []byte, v []byte) (err error) {
	defer tab.psync.Done()

	ls := tab.stores[len(tab.stores)-1]
	err = ls.Put(t, k, v)
	for err != nil {
		if err == log.ELogFull {
			cidx := len(tab.stores) - 1
			/* will block at Lock(); hence i am done */
			tab.ssync.Done()

			tab.Lock()
			if cidx == len(tab.stores)-1 {
				tab.stop()

				/* wait for other puts to block / finish. */
				tab.ssync.Wait()
				/* wait for running gets to finish */
				tab.gsync.Wait()

				if cidx >= cap(tab.stores) {
					return log.ELogFull //should make sortedstore before this could even happen
				}
				var s Store
				if s, err = openStore(tab.path+SName+strconv.Itoa(cidx+1), tab.conf, true); err != nil {
					return
				}
				tab.stores = append(tab.stores, s)

				/* asynchornous convert goroutine */
				tab.csync.Add(1)
				go tab.mkHStoreFn(cidx)

				tab.mayResume()
			}
			tab.Unlock()

			tab.ssync.Add(1)
			ls = tab.stores[len(tab.stores)-1]
			err = ls.Put(t, k, v)
		} else {
			break
		}
	}

	tab.ssync.Done()
	return
}

func (tab *Tablet) mkHStoreFn(idx int) {
	defer tab.csync.Done()

	/* This ensures only one conversion goroutine is running at any time */
	tab.Lock()
	defer tab.Unlock()

	ls, ok := tab.stores[idx].(*MutableStore)
	if !ok {
		panic(fmt.Errorf("tab.stores[%d] is not of type MutableStore", idx))
	}
	err := MakeHashStore(ls)
	if err != nil {
		panic(err)
	}

	var f *os.File
	if f, err = os.Create(ls.path + string(os.PathSeparator) + HFile); err == nil {
		f.Close()
		err = os.Remove(ls.path + string(os.PathSeparator) + LFile)
	}
	if err == nil {
		tab.stop()

		/* wait for running gets to finish */
		tab.gsync.Wait()

		sn := ls.path
		ls.Close()
		hs, err := openStore(sn, tab.conf, false)
		if err != nil {
			return
		}
		tab.stores[idx] = hs

		tab.mayResume()
	} else {
		panic(err)
	}
}

func (tab *Tablet) Do(req Request) (resC chan Result) {
	resC = make(chan Result)
	tab.reqQ <- internalReq{req, resC}

	return
}

func (tab *Tablet) process(r internalReq) {
	req := r.req
	if req == nil {
		return
	}

	switch req.(type) {
	case *ReadRequest:
		tab.limit <- struct{}{}
		tab.gsync.Add(1)
		go func() {
			rr := req.(*ReadRequest)
			tʹ, v, err := tab.get(rr.T, rr.K)
			r.resC <- ReadResult{tʹ, v, ErrVal{err}}
			<-tab.limit
		}()
	case *WriteRequest:
		tab.limit <- struct{}{}
		tab.psync.Add(1)
		tab.ssync.Add(1)
		go func() {
			wr := req.(*WriteRequest)
			err := tab.put(wr.T, wr.K, wr.V)
			r.resC <- ErrVal{err}
			<-tab.limit
		}()
	default:
	}
}

// stop() signals reqHandler() to not accept new requests.
// It must have an associated mayResume() call.
func (tab *Tablet) stop() {
	tab.state <- stopEvent
}

// mayResume() signals reqHandler() that it may accept new requests.
// It *MUST* have an associated stop() call.
func (tab *Tablet) mayResume() {
	tab.state <- resumeEvent
}

// stopAndWait() signals reqHandler() to not accept new requests and waits for all running operations to finish.
// It must have an associated mayResume() call.
func (tab *Tablet) stopAndWait() {
	tab.stop()
	tab.gsync.Wait()
	tab.psync.Wait()
	tab.csync.Wait()
}

// all requests are given a new goroutine to process.
func (tab *Tablet) reqHandler() {
	reqQ := tab.reqQ
	stoppers := 0
	for {
		select {
		case e, ok := <-tab.state:
			if !ok {
				break
			}

			switch e {
			case stopEvent:
				stoppers++
				reqQ = nil
			case resumeEvent:
				if stoppers > 0 {
					stoppers--
				} else {
					panic(fmt.Errorf("resumeEvent sent without associated stopEvent"))
				}
				if stoppers == 0 {
					reqQ = tab.reqQ
				}
			}
		case rq, ok := <-reqQ:
			if ok {
				tab.process(rq)
			} else {
				go func() {
					tab.gsync.Wait()
					tab.psync.Wait()
					tab.csync.Wait()
					close(tab.quit)
				}()

				/* Set both nil */
				reqQ = nil
				tab.reqQ = nil
			}
		case <-tab.quit:
			goto reqLoopExit
		}
	}
reqLoopExit:
}

func (tab *Tablet) Snapshot(w io.Writer) (err error) {
	tab.stopAndWait()
	defer tab.mayResume()

	gzw := gzip.NewWriter(w)
	arw := tar.NewWriter(gzw)
	for _, s := range tab.stores {
		if err = s.Snapshot(arw); err != nil {
			break
		}
	}

	if err = arw.Close(); err == nil {
		err = gzw.Close()
	}

	return
}

func (tab *Tablet) Restore(r io.Reader) (err error) {
	tab.stopAndWait()
	defer tab.mayResume()

	for _, s := range tab.stores {
		s.Close()
		s.Remove()
	}

	gzrd, err := gzip.NewReader(r)
	if err != nil {
		return
	}
	arr := tar.NewReader(gzrd)

	for {
		var thdr *tar.Header
		thdr, err = arr.Next()

		if err == io.EOF {
			break
		} else if err != nil {
			return
		}

		var i int
		for i = len(thdr.Name) - 1; i >= 0 && thdr.Name[i] != '/'; i-- {
		}

		if err = os.MkdirAll(tab.path+thdr.Name[:i], 0755); err != nil && !os.IsExist(err) {
			break
		}

		var f *os.File
		f, err = os.OpenFile(tab.path+thdr.Name, os.O_CREATE|os.O_RDWR, 0600)
		if err != nil {
			break
		}

		if _, err = io.Copy(f, arr); err != nil {
			break
		}
		f.Close()
	}

	tab.stores = tab.stores[:0]
	for i := 0; i < cap(tab.stores); i++ {
		var s Store
		sn := tab.path + SName + strconv.Itoa(i)
		if s, err = openStore(sn, tab.conf, false); err != nil {
			break
		}
		tab.stores = append(tab.stores, s)
	}
	if len(tab.stores) >= 0 {
		err = nil
	}

	return
}

// Caller should guarantee no new requests will be sent after the call to Close(). Else it will panic.
func (tab *Tablet) Close() (err error) {
	select {
	case <-tab.quit:
		return
	default:
		close(tab.reqQ)
	}
	/* Wait for all goroutines to finish */
	<-tab.quit

	for i := 0; i < len(tab.stores); i++ {
		if err = tab.stores[i].Close(); err != nil {
			break
		}
	}
	return
}
