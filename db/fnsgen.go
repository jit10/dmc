/* Generated Code; DO *NOT* EDIT */

package db

import (
	"io"
)

const (
	datumHeaderSize      = 8
	lsHeaderSize         = 25
	storeHeaderSize      = 8
	versionSize          = 16
	versionHeaderSize    = 8
	indexTableFooterSize = 8
)

func (t *datumHeader) Marshal(wire io.Writer) error {
	var err error
	var b [8]byte
	var bs []byte
	bs = b[:8]
	tmp32 := t.klen
	bs[0] = byte(tmp32)
	bs[1] = byte(tmp32 >> 8)
	bs[2] = byte(tmp32 >> 16)
	bs[3] = byte(tmp32 >> 24)
	tmp32 = t.vlen
	bs[4] = byte(tmp32)
	bs[5] = byte(tmp32 >> 8)
	bs[6] = byte(tmp32 >> 16)
	bs[7] = byte(tmp32 >> 24)
	_, err = wire.Write(bs)
	return err
}

func (t *datumHeader) Unmarshal(wire io.Reader) error {
	var b [8]byte
	var bs []byte
	bs = b[:8]
	if _, err := io.ReadAtLeast(wire, bs, 8); err != nil {
		return err
	}
	t.klen = uint32((uint32(bs[0]) | (uint32(bs[1]) << 8) | (uint32(bs[2]) << 16) | (uint32(bs[3]) << 24)))
	t.vlen = uint32((uint32(bs[4]) | (uint32(bs[5]) << 8) | (uint32(bs[6]) << 16) | (uint32(bs[7]) << 24)))
	return nil
}

func (t *lsHeader) Marshal(wire io.Writer) error {
	var err error
	var b [25]byte
	var bs []byte
	bs = b[:25]
	tmp32 := t.klen
	bs[0] = byte(tmp32)
	bs[1] = byte(tmp32 >> 8)
	bs[2] = byte(tmp32 >> 16)
	bs[3] = byte(tmp32 >> 24)
	tmp32 = t.vlen
	bs[4] = byte(tmp32)
	bs[5] = byte(tmp32 >> 8)
	bs[6] = byte(tmp32 >> 16)
	bs[7] = byte(tmp32 >> 24)
	bs[8] = byte(t.flags)
	tmp64 := t.prev
	bs[9] = byte(tmp64)
	bs[10] = byte(tmp64 >> 8)
	bs[11] = byte(tmp64 >> 16)
	bs[12] = byte(tmp64 >> 24)
	bs[13] = byte(tmp64 >> 32)
	bs[14] = byte(tmp64 >> 40)
	bs[15] = byte(tmp64 >> 48)
	bs[16] = byte(tmp64 >> 56)
	tmp64 = t.time
	bs[17] = byte(tmp64)
	bs[18] = byte(tmp64 >> 8)
	bs[19] = byte(tmp64 >> 16)
	bs[20] = byte(tmp64 >> 24)
	bs[21] = byte(tmp64 >> 32)
	bs[22] = byte(tmp64 >> 40)
	bs[23] = byte(tmp64 >> 48)
	bs[24] = byte(tmp64 >> 56)
	_, err = wire.Write(bs)
	return err
}

func (t *lsHeader) Unmarshal(wire io.Reader) error {
	var b [25]byte
	var bs []byte
	bs = b[:25]
	if _, err := io.ReadAtLeast(wire, bs, 25); err != nil {
		return err
	}
	t.klen = uint32((uint32(bs[0]) | (uint32(bs[1]) << 8) | (uint32(bs[2]) << 16) | (uint32(bs[3]) << 24)))
	t.vlen = uint32((uint32(bs[4]) | (uint32(bs[5]) << 8) | (uint32(bs[6]) << 16) | (uint32(bs[7]) << 24)))
	t.flags = uint8(bs[8])
	t.prev = int64((uint64(bs[9]) | (uint64(bs[10]) << 8) | (uint64(bs[11]) << 16) | (uint64(bs[12]) << 24) | (uint64(bs[13]) << 32) | (uint64(bs[14]) << 40) | (uint64(bs[15]) << 48) | (uint64(bs[16]) << 56)))
	t.time = int64((uint64(bs[17]) | (uint64(bs[18]) << 8) | (uint64(bs[19]) << 16) | (uint64(bs[20]) << 24) | (uint64(bs[21]) << 32) | (uint64(bs[22]) << 40) | (uint64(bs[23]) << 48) | (uint64(bs[24]) << 56)))
	return nil
}

func (t *storeHeader) Marshal(wire io.Writer) error {
	var err error
	var b [8]byte
	var bs []byte
	bs = b[:8]
	tmp32 := t.power
	bs[0] = byte(tmp32)
	bs[1] = byte(tmp32 >> 8)
	bs[2] = byte(tmp32 >> 16)
	bs[3] = byte(tmp32 >> 24)
	tmp32 = t.slots
	bs[4] = byte(tmp32)
	bs[5] = byte(tmp32 >> 8)
	bs[6] = byte(tmp32 >> 16)
	bs[7] = byte(tmp32 >> 24)
	_, err = wire.Write(bs)
	return err
}

func (t *storeHeader) Unmarshal(wire io.Reader) error {
	var b [8]byte
	var bs []byte
	bs = b[:8]
	if _, err := io.ReadAtLeast(wire, bs, 8); err != nil {
		return err
	}
	t.power = uint32((uint32(bs[0]) | (uint32(bs[1]) << 8) | (uint32(bs[2]) << 16) | (uint32(bs[3]) << 24)))
	t.slots = uint32((uint32(bs[4]) | (uint32(bs[5]) << 8) | (uint32(bs[6]) << 16) | (uint32(bs[7]) << 24)))
	return nil
}

func (t *version) Marshal(wire io.Writer) error {
	var err error
	var b [16]byte
	var bs []byte
	bs = b[:16]
	tmp64 := t.time
	bs[0] = byte(tmp64)
	bs[1] = byte(tmp64 >> 8)
	bs[2] = byte(tmp64 >> 16)
	bs[3] = byte(tmp64 >> 24)
	bs[4] = byte(tmp64 >> 32)
	bs[5] = byte(tmp64 >> 40)
	bs[6] = byte(tmp64 >> 48)
	bs[7] = byte(tmp64 >> 56)
	tmp64 = t.epos
	bs[8] = byte(tmp64)
	bs[9] = byte(tmp64 >> 8)
	bs[10] = byte(tmp64 >> 16)
	bs[11] = byte(tmp64 >> 24)
	bs[12] = byte(tmp64 >> 32)
	bs[13] = byte(tmp64 >> 40)
	bs[14] = byte(tmp64 >> 48)
	bs[15] = byte(tmp64 >> 56)
	_, err = wire.Write(bs)
	return err
}

func (t *version) Unmarshal(wire io.Reader) error {
	var b [16]byte
	var bs []byte
	bs = b[:16]
	if _, err := io.ReadAtLeast(wire, bs, 16); err != nil {
		return err
	}
	t.time = int64((uint64(bs[0]) | (uint64(bs[1]) << 8) | (uint64(bs[2]) << 16) | (uint64(bs[3]) << 24) | (uint64(bs[4]) << 32) | (uint64(bs[5]) << 40) | (uint64(bs[6]) << 48) | (uint64(bs[7]) << 56)))
	t.epos = int64((uint64(bs[8]) | (uint64(bs[9]) << 8) | (uint64(bs[10]) << 16) | (uint64(bs[11]) << 24) | (uint64(bs[12]) << 32) | (uint64(bs[13]) << 40) | (uint64(bs[14]) << 48) | (uint64(bs[15]) << 56)))
	return nil
}

func (t *versionHeader) Marshal(wire io.Writer) error {
	var err error
	var b [8]byte
	var bs []byte
	bs = b[:8]
	tmp32 := t.klen
	bs[0] = byte(tmp32)
	bs[1] = byte(tmp32 >> 8)
	bs[2] = byte(tmp32 >> 16)
	bs[3] = byte(tmp32 >> 24)
	tmp32 = t.nvers
	bs[4] = byte(tmp32)
	bs[5] = byte(tmp32 >> 8)
	bs[6] = byte(tmp32 >> 16)
	bs[7] = byte(tmp32 >> 24)
	_, err = wire.Write(bs)
	return err
}

func (t *versionHeader) Unmarshal(wire io.Reader) error {
	var b [8]byte
	var bs []byte
	bs = b[:8]
	if _, err := io.ReadAtLeast(wire, bs, 8); err != nil {
		return err
	}
	t.klen = uint32((uint32(bs[0]) | (uint32(bs[1]) << 8) | (uint32(bs[2]) << 16) | (uint32(bs[3]) << 24)))
	t.nvers = uint32((uint32(bs[4]) | (uint32(bs[5]) << 8) | (uint32(bs[6]) << 16) | (uint32(bs[7]) << 24)))
	return nil
}

func (t *indexTableFooter) Marshal(wire io.Writer) error {
	var err error
	var b [8]byte
	var bs []byte
	bs = b[:8]
	tmp64 := t.pos
	bs[0] = byte(tmp64)
	bs[1] = byte(tmp64 >> 8)
	bs[2] = byte(tmp64 >> 16)
	bs[3] = byte(tmp64 >> 24)
	bs[4] = byte(tmp64 >> 32)
	bs[5] = byte(tmp64 >> 40)
	bs[6] = byte(tmp64 >> 48)
	bs[7] = byte(tmp64 >> 56)
	_, err = wire.Write(bs)
	return err
}

func (t *indexTableFooter) Unmarshal(wire io.Reader) error {
	var b [8]byte
	var bs []byte
	bs = b[:8]
	if _, err := io.ReadAtLeast(wire, bs, 8); err != nil {
		return err
	}
	t.pos = int64((uint64(bs[0]) | (uint64(bs[1]) << 8) | (uint64(bs[2]) << 16) | (uint64(bs[3]) << 24) | (uint64(bs[4]) << 32) | (uint64(bs[5]) << 40) | (uint64(bs[6]) << 48) | (uint64(bs[7]) << 56)))
	return nil
}
