package db

import (
	"crypto/rand"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"testing"
)

const (
	SnapPath  = "/tmp/tabSnap"
	MaxItems  = 1024 * 1024
	ValueSize = 64
)

var tab *Tablet
var path string

func randData() (d []byte) {
	d = make([]byte, ValueSize)
	io.ReadFull(rand.Reader, d)
	return
}

func init() {
	conf, err := ParseDBConfig("testing/config.xml")
	if err != nil {
		log.Print(err)
		log.Println(" : Using Default Config")
	}
	tab, err = OpenTablet("testtab", conf)
	if err != nil {
		log.Fatalln(err)
	}

	path = conf.DBPath
}

func BenchmarkWrite(b *testing.B) {
	b.N = MaxItems

	var wg sync.WaitGroup

	wg.Add(MaxItems)
	v := randData()
	for i := 0; i < MaxItems; i++ {
		rq := &WriteRequest{T: 0, K: []byte(strconv.Itoa(i)), V: v}
		resC := tab.Do(rq)
		go func(i int) {
			e := <-resC
			if e.Error() != nil {
				log.Fatalln(e.Error())
			}
			wg.Done()
		}(i)
	}

	w := make(chan bool)
	go func() {
		wg.Wait()
		w <- true
	}()
	<-w
}

func BenchmarkSnapshot(b *testing.B) {
	fsnap, err := os.Create(SnapPath)
	defer fsnap.Close()

	if err != nil {
		b.Error(err)
	}
	if err = tab.Snapshot(fsnap); err != nil {
		b.Error(err)
	}
}

func BenchmarkRestore(b *testing.B) {
	fsnap, err := os.Open(SnapPath)
	defer fsnap.Close()

	if err != nil {
		b.Error(err)
	}

	if err = tab.Restore(fsnap); err != nil {
		b.Error(err)
	}
}

func BenchmarkRead(b *testing.B) {
	b.N = MaxItems

	var wg sync.WaitGroup

	wg.Add(MaxItems)
	for i := 0; i < MaxItems; i++ {
		rq := &ReadRequest{0, []byte(strconv.Itoa(i))}
		resC := tab.Do(rq)
		go func(i int) {
			res := (<-resC).(ReadResult)
			if res.error != nil {
				b.Error(res.error)
			}
			wg.Done()
		}(i)
	}

	w := make(chan bool)
	go func() {
		wg.Wait()
		w <- true
	}()
	<-w
}

func BenchmarkFini(b *testing.B) {
	if err := tab.Close(); err != nil {
		log.Fatalln(err)
	}

	os.RemoveAll(path)
	os.Remove(SnapPath)
}
