package db

import (
	"archive/tar"
	"encoding/binary"
	//	"fmt"
	"bufio"
	"bytes"
	"io"
	"os"
	"sort"

	. "bitbucket.org/jpathy/dmc/cuckoo"
	"bitbucket.org/jpathy/dmc/log"
	"bitbucket.org/jpathy/dmc/util"
)

const (
	fileBufferSize = 8192
)

type offsets []uint32

/* Satisfy cuckoo.PositionTable interface */

func (offtbl offsets) GetPosAt(idx int64) (pos int64) {
	return int64(offtbl[idx])
}

func (offtbl offsets) PutPosAt(idx, pos int64) error {
	offtbl[idx] = uint32(pos)
	return nil
}

/* ... */

type MutableStore struct {
	path   string
	t      *CuckooTable
	offtbl offsets
	log.Mutable
}

/* Sort Interface (sort by key) */
type keyPosTuples []struct {
	k   []byte
	pos int64
}

func (knp keyPosTuples) Len() int {
	return len(knp)
}

func (knp keyPosTuples) Less(i, j int) bool {
	return bytes.Compare(knp[i].k, knp[j].k) == -1
}

func (knp keyPosTuples) Swap(i, j int) {
	knp[i], knp[j] = knp[j], knp[i]
}

/* ... */

func MakeHashStore(s *MutableStore) (err error) {
	if err = s.Sync(); err != nil {
		return
	}

	/* Sort the tuples of (key, position of last version) */
	var knp keyPosTuples
	for i := uint32(0); i < uint32(len(s.offtbl)); i++ {
		if !s.t.IsEmpty(i) {
			var hdr lsHeader
			epos := int64(s.offtbl[i])
			if err = s.ReadHeader(&hdr, epos); err != nil {
				return
			}
			k := make([]byte, hdr.klen)
			if err = s.ReadData(k, epos+lsHeaderSize); err != nil {
				return
			}
			knp = append(knp, struct {
				k   []byte
				pos int64
			}{k, epos})
		}
	}
	sort.Sort(knp)

	/* New ts.log will have the versions of each key */
	f, err := os.OpenFile(s.path+string(os.PathSeparator)+TSLogName, os.O_CREATE|os.O_RDWR, 0600)
	defer f.Close()
	if err != nil {
		return
	}
	tslog := bufio.NewWriterSize(f, fileBufferSize)

	tsidx := int64(0)
	o2n := make(map[int64]int64)
	for i := 0; i < len(knp); i++ {

		/* Collect the versions of this key */
		var hdr lsHeader
		var vers []version
		epos := knp[i].pos
		for {
			if err = s.ReadHeader(&hdr, epos); err != nil {
				return
			}
			vers = append(vers, version{hdr.time, epos})
			if hdr.prev == -1 {
				break
			}
			epos = hdr.prev
		}

		/* Write versionHeader + key + versions */
		vhdr := versionHeader{uint32(len(knp[i].k)), uint32(len(vers))}
		if err = vhdr.Marshal(tslog); err != nil {
			return
		}
		if err = binary.Write(tslog, binary.LittleEndian, knp[i].k); err != nil {
			return
		}
		for i := range vers {
			if err = vers[i].Marshal(tslog); err != nil {
				return
			}
		}

		/* map old positions in db.log to new positions in ts.log */
		o2n[knp[i].pos] = tsidx
		tsidx += int64(versionHeaderSize + int(vhdr.klen) + versionSize*len(vers))
	}

	/* Write the new offset table in hash order */
	for i, off := range s.offtbl {
		var npos int64
		if s.t.IsEmpty(uint32(i)) {
			npos = -1
		} else {
			npos = o2n[int64(off)]
		}
		if err = binary.Write(tslog, binary.LittleEndian, npos); err != nil {
			return
		}
	}

	/* Write the footer containing the position of offtbl in ts.log */
	if err == nil {
		ftr := indexTableFooter{pos: tsidx}
		if err = ftr.Marshal(tslog); err == nil {
			tslog.Flush()
		}
	}
	return
}

/* Satisfy cuckoo.CuckooStorage interfeace */

func (s *MutableStore) CompareKeyAt(k []byte, pos int64) (e bool) {
	var hdr lsHeader
	if err := s.ReadHeader(&hdr, pos); err == nil {
		if hdr.klen != uint32(len(k)) {
			e = false
		}
		rk := make([]byte, hdr.klen)
		if err = s.ReadData(rk, int64(pos+lsHeaderSize)); err == nil {
			e = bytes.Compare(k, rk) == 0
		}
	}
	return
}

func (s *MutableStore) KeyIterator() func() (pos int64, k []byte, err error) {
	ipos := int64(storeHeaderSize)
	return func() (pos int64, k []byte, err error) {
		var hdr lsHeader
		for {
			if ipos >= s.Size() {
				return pos, nil, io.EOF
			}
			if err = s.ReadHeader(&hdr, ipos); err == nil {
				if hdr.flags&InvEntry == InvEntry {
					pos = ipos
					ipos += int64(lsHeaderSize + hdr.klen + hdr.vlen)
					continue
				}
				k = make([]byte, hdr.klen)
				err = s.ReadData(k, int64(ipos+lsHeaderSize))
			}
			if err == nil {
				pos = ipos
				ipos += int64(lsHeaderSize + hdr.klen + hdr.vlen)
			}
			break
		}
		return
	}
}

func (s *MutableStore) PosTbl() PositionTable {
	return s.offtbl
}

/* ... */

/* Satisfy db.Store interface */

func (s *MutableStore) GetLatest(k []byte) (t int64, v []byte, err error) {
	off := s.t.PositionOf(k)
	if off == -1 {
		return
	}

	var hdr lsHeader
	if err = s.ReadHeader(&hdr, off); err == nil {
		if err = s.ReadHeader(&hdr, off); err != nil {
			return
		}
		t = hdr.time
		v = make([]byte, hdr.vlen)
		err = s.ReadData(v, off+int64(lsHeaderSize+hdr.klen))
	}
	return
}

func (s *MutableStore) Get(t int64, k []byte, nvers *int) (tʹ int64, v []byte, err error) {
	off := s.t.PositionOf(k)
	if off == -1 {
		return
	}

	var hdr lsHeader
	for i := 0; i < *nvers; i++ {
		*nvers--
		if err = s.ReadHeader(&hdr, off); err != nil {
			break
		}
		if t >= hdr.time {
			v = make([]byte, hdr.vlen)
			tʹ = hdr.time
			err = s.ReadData(v, off+int64(lsHeaderSize+hdr.klen))
			break
		} else {
			if hdr.prev == -1 {
				break
			} else {
				off = hdr.prev
			}
		}
	}
	return
}

func (s *MutableStore) Put(t int64, k []byte, v []byte) (err error) {
	off := s.t.PositionOf(k)
	hdr := &lsHeader{
		flags: 0,
		datumHeader: datumHeader{
			klen: uint32(len(k)),
			vlen: uint32(len(v)),
		},
		prev: off,
		time: t,
	}
	pos, err := s.Write(hdr, k, v)
	if err == nil {
		if off == -1 {
			err = s.t.PositionAt(k, pos)
		}
		if err == ETableFull {
			if _, err = s.WriteAt([]byte{InvEntry}, pos+datumHeaderSize); err != nil {
				panic(err)
			}
			err = log.ELogFull
		}
	}
	return
}

func (s *MutableStore) Snapshot(w *tar.Writer) (err error) {
	if err = w.WriteHeader(&tar.Header{
		Name: util.Basename(s.path) + string(os.PathSeparator) + DBLogName,
		Size: s.Size(),
	}); err != nil {
		return
	}

	err = s.Dup(w)
	return
}

func (s *MutableStore) Remove() error {
	return os.RemoveAll(s.path)
}

/* ... */

func OpenLogStore(path string, conf *Config) (s *MutableStore, err error) {
	lg, err := log.OpenMutable(path + string(os.PathSeparator) + DBLogName)
	if err != nil {
		return
	}

	var shdr storeHeader
	if lg.Size() == 0 {
		shdr = storeHeader{
			power: conf.HashPower,
			slots: conf.Slots,
		}

		_, err = lg.Write(&shdr, nil)
	} else {
		err = lg.ReadHeader(&shdr, 0)
	}

	if err != nil {
		return
	}

	s = &MutableStore{
		offtbl:  make([]uint32, shdr.slots*(1<<shdr.power)),
		Mutable: lg,
	}
	t, err := NewCuckooTable(shdr.power, shdr.slots, s, false)
	if err != nil {
		return nil, err
	}
	s.t = t
	s.path = path
	return
}
