package db

const (
	DBLogName = "db.log"
	TSLogName = "ts.log"
)

const (
	InvEntry = 0x1
)

type datumHeader struct {
	klen uint32
	vlen uint32
}

func (hdr *datumHeader) Size() int {
	return datumHeaderSize
}

type lsHeader struct {
	datumHeader
	flags uint8
	prev  int64
	time  int64
}

func (hdr *lsHeader) Size() int {
	return lsHeaderSize
}

type storeHeader struct {
	power uint32
	slots uint32
}

func (hdr *storeHeader) Size() int {
	return storeHeaderSize
}

type version struct {
	time int64
	epos int64
}

type versionHeader struct {
	klen  uint32
	nvers uint32
}

type indexTableFooter struct {
	pos int64
}
