package db

import (
	"archive/tar"
)

type Store interface {
	GetLatest(k []byte) (t int64, v []byte, err error)
	Get(t int64, k []byte, nvers *int) (tʹ int64, v []byte, err error)

	Put(t int64, k []byte, v []byte) (err error)

	Snapshot(*tar.Writer) error

	Close() error
	Remove() error
}
