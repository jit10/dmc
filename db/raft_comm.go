package db

import (
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"time"

	"bitbucket.org/jpathy/dmc/ndb"
	"bitbucket.org/jpathy/raft"
)

const (
	dialTimeOut = 100 * time.Millisecond
)

type tcpComm struct {
	timeout time.Duration
	uid     string
	net.Listener
	ns *ndb.Client
}

func (comm *tcpComm) String() string {
	return "tcp"
}

func (comm *tcpComm) Dial(rid string) (t raft.Transporter, err error) {
	h, _, p := id2Addr(comm.ns, rid)

	raddr := h + ":" + strconv.Itoa(int(p))
	c, err := net.DialTimeout("tcp", raddr, comm.timeout)
	if err != nil {
		return
	}

	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]
	sl := binary.PutUvarint(bs, uint64(len(comm.uid)))
	bs = append(bs[:sl], []byte(comm.uid)...)
	if _, err = c.Write(bs); err != nil {
		return
	}

	if err == nil {
		t = raft.NewNetTransporter(rid, c)
	}

	return
}

func (comm *tcpComm) Accept() (t raft.Transporter, err error) {
	c, err := comm.Listener.Accept()
	if err != nil {
		return
	}

	var ba [binary.MaxVarintLen64]byte
	var slen uint64
	bs := ba[:]
	k, _ := c.Read(bs)
	if k > 0 {
		slen, k = binary.Uvarint(bs[:k])
	}
	if k <= 0 {
		err = fmt.Errorf("failed to read length of uid")
		return
	}
	bs = append(bs, make([]byte, int(slen)-len(ba)+k)...)
	if _, err = c.Read(bs[len(ba):]); err != nil {
		return
	}

	uid := string(bs[k:])
	if err == nil {
		t = raft.NewNetTransporter(uid, c)
	}
	return
}

func (comm *tcpComm) Close() error {
	return comm.Listener.Close()
}

func commFunc(conf *Config) (f func(string) (raft.Communication, error)) {
	ns := ndb.NewClient(&conf.Config)

	f = func(uid string) (raft.Communication, error) {
		conlr, err := net.Listen("tcp", ":"+strconv.Itoa(int(conf.Port)+1))
		if err != nil {
			return nil, err
		}

		comm := &tcpComm{
			uid:      uid,
			Listener: conlr,
			ns:       ns,
		}

		return comm, nil
	}
	return
}
