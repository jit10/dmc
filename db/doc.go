package db

/*
Tablet goroutines can be divided into three groups of Operations (get, put, convert) running asynchronously.
*/

/*
Tablet synchronization Primitves

gsync, psync, csync: get, put, convert goroutine synchronizers.
ssync: waits for all put goroutines to block/finish. This is a special case to wait for all puts before modifying tab.stores.
state: to stop and resume accepting new requests by sending appropriate events.
quit: reqhandler exit event.
(see reqHandler())
*/

/*
reqHandler

state channel is used to recieve events to stop and resume accepting requests. It counts the no of stop events recieved and only resumes when resume events associated with all stop events have been received.
Hence each stop()/stopAndWait() must have associated mayResume() and vice-versa.
*/

/*
Notes

stopAndWait() can be used as a stop-the-world mechanism. It can be useful to take snapshots for example.
Order of synchronization operations matter through-out the code. Any change requires careful examination.
*/

/*
Read-Only Requests

To assign TS to read-only requests has to be issued at the leader.
Currently we do a raft-write of TS to make sure the leader has seen all previous writes.
This has to be changed so as to avoid a log-write for chosing TS for RD-only request. (Needs more power from the raft library).
*/
