package db

import (
	. "bitbucket.org/jpathy/dmc/cuckoo"
)

func OpenHashStore(path string) (s *ImmutableStore, err error) {
	s, err = immutableStore(path)
	if err != nil {
		return nil, err
	}

	var shdr storeHeader
	if err = s.ReadHeader(&shdr, 0); err != nil {
		return
	}

	t, err := NewCuckooTable(shdr.power, shdr.slots, s, true)
	if err != nil {
		return nil, err
	}
	s.t = t
	return
}
