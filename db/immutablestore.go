package db

import (
	"archive/tar"
	"bytes"
	"io"
	"os"
	"sort"

	. "bitbucket.org/jpathy/dmc/cuckoo"
	"bitbucket.org/jpathy/dmc/log"
	"bitbucket.org/jpathy/dmc/util"
)

const (
	VersReadSize = 1024
)

type memIndexTable interface {
	PositionOf(k []byte) (pos int64)
}

type ImmutableStore struct {
	path  string
	t     memIndexTable
	vslog versionLog
	log.Immutable
}

/* satsify cuckoo.CuckooStorage interface */

func (s *ImmutableStore) CompareKeyAt(k []byte, pos int64) (e bool) {
	var vhdr versionHeader
	if err := s.vslog.readHeader(&vhdr, pos); err != nil {
		return
	}
	if vhdr.klen != uint32(len(k)) {
		return
	}
	tk := make([]byte, len(k))
	if err := s.vslog.readKey(tk, pos+versionHeaderSize); err != nil {
		return
	}
	e = bytes.Compare(k, tk) == 0
	return
}

func (s *ImmutableStore) KeyIterator() func() (pos int64, k []byte, err error) {
	ipos := int64(0)
	return func() (pos int64, k []byte, err error) {
		if ipos >= s.vslog.sizeOfftbl {
			return 0, nil, io.EOF
		}

		vpos := s.vslog.GetPosAt(ipos)
		if vpos != -1 {
			var vhdr versionHeader
			if err = s.vslog.readHeader(&vhdr, vpos); err == nil {
				k = make([]byte, vhdr.klen)
				err = s.vslog.readKey(k, vpos+versionHeaderSize)
			}
		}
		if err == nil {
			pos = vpos
			ipos++
		}
		return
	}
}

func (s *ImmutableStore) PosTbl() PositionTable {
	return &s.vslog
}

/* .. */

/* Satisfy db.Store Interface */

func (s *ImmutableStore) GetLatest(k []byte) (t int64, v []byte, err error) {
	vpos := s.t.PositionOf(k)
	if vpos == -1 {
		return
	}

	var ver version
	if err = s.vslog.lastVersion(&ver, vpos); err != nil {
		return
	}

	var hdr lsHeader
	if err = s.ReadHeader(&hdr, ver.epos); err != nil {
		return
	}
	t = ver.time
	v = make([]byte, hdr.vlen)
	err = s.ReadData(v, ver.epos+lsHeaderSize+int64(hdr.klen))
	return
}

func (s *ImmutableStore) Get(t int64, k []byte, nvers *int) (tʹ int64, v []byte, err error) {
	pos := s.t.PositionOf(k)
	if pos == -1 {
		return
	}

	var vhdr versionHeader
	if err = s.vslog.readHeader(&vhdr, pos); err != nil {
		return
	}
	nvs := *nvers
	if nvs > int(vhdr.nvers) {
		nvs = int(vhdr.nvers)
		*nvers -= nvs
	}

	vpos := pos + versionHeaderSize + int64(vhdr.klen)
	lpos := int64(-1)
	varr := [VersReadSize]version{}
	vers := varr[:]
	searchFunc := func(i int) bool { return t >= vers[i].time }
	for ; nvs > 0; nvs -= VersReadSize {
		if nvs < cap(vers) {
			vers = vers[:nvs]
		} else {
			vers = vers[:cap(vers)]
		}
		if err = s.vslog.readVersions(vers, vpos); err != nil {
			return
		}
		if n := sort.Search(len(vers), searchFunc); n < len(vers) {
			tʹ = vers[n].time
			lpos = vers[n].epos
			break
		}
	}
	if lpos == -1 {
		return
	}

	var hdr lsHeader
	if err = s.ReadHeader(&hdr, lpos); err != nil {
		return
	}
	v = make([]byte, hdr.vlen)
	err = s.ReadData(v, lpos+lsHeaderSize+int64(hdr.klen))
	return
}

func (s *ImmutableStore) Put(t int64, k []byte, v []byte) (err error) {
	return EPut
}

func (s *ImmutableStore) Snapshot(w *tar.Writer) (err error) {
	if err = w.WriteHeader(&tar.Header{
		Name: util.Basename(s.path) + string(os.PathSeparator) + DBLogName,
		Size: s.Size(),
	}); err != nil {
		return
	}

	if err = s.Dup(w); err != nil {
		return
	}

	fi, err := s.vslog.Stat()
	if err != nil {
		return
	}

	if err = w.WriteHeader(&tar.Header{
		Name: util.Basename(s.path) + string(os.PathSeparator) + TSLogName,
		Size: fi.Size(),
	}); err != nil {
		return
	}
	if _, err = io.Copy(w, s.vslog); err != nil {
		return
	}

	_, err = s.vslog.Seek(0, os.SEEK_SET)
	return
}

func (s *ImmutableStore) Close() (err error) {
	if err = s.Immutable.Close(); err == nil {
		err = s.vslog.Close()
	}
	return
}

func (s *ImmutableStore) Remove() error {
	return os.RemoveAll(s.path)
}

/* ... */

func immutableStore(path string) (*ImmutableStore, error) {
	lg, err := log.OpenImmutable(path + string(os.PathSeparator) + DBLogName)
	if err != nil {
		return nil, err
	}

	vl, err := openVersionLog(path)
	if err != nil {
		return nil, err
	}

	return &ImmutableStore{path: path, vslog: vl, Immutable: lg}, nil
}
