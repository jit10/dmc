package db

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/jpathy/dmc/ndb"
	"bitbucket.org/jpathy/raft"
)

const (
	WaitToleranceRatio = 10 // i.e. 10%.
	WeightRatio        = 4  // i.e. 0.25.
	UnitTm             = 500 * time.Microsecond

	MaxBatchSize     = 100
	MaxConnPerClient = 10
)

var (
	ErrReq      = "invalid Request"
	ErrPost     = "http client failed to post"
	ErrResValue = "invalid result value recieved"
	ErrResType  = "result type mismatch with request type"
)

type jsonEncoder struct {
	*json.Encoder
	buf bytes.Buffer
}

var encoderPool = &sync.Pool{
	New: func() interface{} {
		jenc := new(jsonEncoder)
		jenc.Encoder = json.NewEncoder(&jenc.buf)
		return jenc
	},
}

type Response struct {
	Res Result
	Err string
}

type clientRequest struct {
	Typ string  `json:"Type"`
	Req Request `json:"Request"`
}

type clientResult struct {
	Typ string          `json:"Type"`
	Msg json.RawMessage `json:"Result"`
	Err string          `json:"Error"`
}

type internalClientRequest struct {
	Ts    int64
	Req   clientRequest
	RespC chan Response
}

type batchRequest struct {
	Ts   int64
	Reqs []clientRequest
}

func decodeHttpResponse(r io.Reader) (cres []clientResult, err error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return
	}

	err = json.Unmarshal(b, &cres)
	return
}

func decodeClientResult(typ string, msg json.RawMessage) (res Result, err error) {
	switch typ {
	case ReadType:
		res = new(ReadResult)
	case WriteType:
		res = new(WriteResult)
	default:
		err = fmt.Errorf(ErrResValue)
		return
	}

	err = json.Unmarshal(msg, res)
	return
}

type Client struct {
	sync.Mutex
	wg sync.WaitGroup
	qC chan bool

	ldrUrl string
	ns     *ndb.Client

	avgWaitDur int64
	batchUnits uint
	batchTm    *time.Timer

	reqC    chan internalClientRequest
	batchC  chan *batchRequest
	brqPool *sync.Pool
	respMap map[clientRequest]chan Response

	htc *http.Client
}

func NewClient(confPath string) (c *Client, err error) {
	conf, err := ndb.ParseConfig(confPath)
	if err != nil {
		return
	}

	ns := ndb.NewClient(conf)

	c = &Client{
		batchUnits: 1,
		batchTm:    time.NewTimer(UnitTm),
		reqC:       make(chan internalClientRequest, MaxBatchSize),
		batchC:     make(chan *batchRequest, MaxConnPerClient),
		brqPool: &sync.Pool{
			New: func() interface{} { return make([]clientRequest, 0, MaxBatchSize) },
		},
		respMap: make(map[clientRequest]chan Response),
		qC:      make(chan bool),
		ns:      ns,
		htc: &http.Client{
			Timeout: 5 * time.Minute,
		},
	}

	c.wg.Add(1)
	go c.reqHandler()

	return
}

func (c *Client) Close() {
	close(c.qC)
	c.wg.Wait()
}

func (c *Client) post(urlpath string, req interface{}) (resp *http.Response, err error) {
	jenc := encoderPool.Get().(*jsonEncoder)
	defer func() {
		jenc.buf.Reset()
		encoderPool.Put(jenc)
	}()

	if err = jenc.Encode(req); err != nil {
		return
	}

	c.Lock()
	if c.ldrUrl == "" {
		var infos []*ndb.SMInfo
		infos, err = c.ns.Query("")
		if err != nil || len(infos) == 0 {
			err = fmt.Errorf(ErrPost)
		} else {
			info := infos[rand.Intn(len(infos))]
			c.ldrUrl = "http://" + info.Host + ":" + strconv.Itoa(int(info.SMPort))
		}
	}
	curLeader := c.ldrUrl
	c.Unlock()

	if err != nil {
		return
	}

	if resp, err = c.htc.Post(curLeader+urlpath, "application/json", &jenc.buf); err != nil {
		err = fmt.Errorf(ErrPost)
	}

	return
}

func (c *Client) buildBatch() (breq *batchRequest) {
	c.Lock()
	if !c.batchTm.Reset(time.Duration(c.batchUnits) * UnitTm) {
		select {
		case <-c.batchTm.C:
		default:
		}
	}
	c.Unlock()

	select {
	case breq = <-c.batchC:

	default:
		var fts int64
		reqs := c.brqPool.Get().([]clientRequest)
		for {
			if len(reqs) == cap(reqs) {
				break
			}
			select {
			case rq := <-c.reqC:
				if len(reqs) == 0 {
					fts = rq.Ts
				}
				reqs = append(reqs, rq.Req)
				c.Lock()
				c.respMap[rq.Req] = rq.RespC
				c.Unlock()

			case <-c.batchTm.C:
				goto endBatchLoop
			default:
			}
		}
	endBatchLoop:
		if len(reqs) == 0 {
			c.brqPool.Put(reqs)
			break
		}
		breq = &batchRequest{
			Ts:   fts,
			Reqs: reqs,
		}
	}

	return
}

func (c *Client) process(breq *batchRequest, limitC <-chan struct{}) {
	defer func() {
		<-limitC
		c.wg.Done()
	}()

	resp, err := c.post(storePath, breq.Reqs)
	defer func() {
		if resp != nil {
			resp.Body.Close()
		}
	}()

	if err != nil {
		c.batchC <- breq
		return
	}

	if resp.StatusCode == http.StatusTemporaryRedirect {
		url, _ := resp.Location()
		c.ldrUrl = url.String()
	}

	ress, err := decodeHttpResponse(resp.Body)
	if err != nil {
		c.batchC <- breq
		return
	}

	var n int
	var rs clientResult
	var nreqs []clientRequest
	for n, rs = range ress {
		reqn := breq.Reqs[n]
		if rs.Typ != reqn.Typ {
			rs.Err = ErrResType
		}

		if rs.Err == "" || rs.Err != ErrTemp {
			var res Result
			res, err = decodeClientResult(rs.Typ, rs.Msg)
			if err != nil {
				rs.Err = err.Error()
			} else {
				c.Lock()
				respC := c.respMap[reqn]
				delete(c.respMap, reqn)
				c.Unlock()
				respC <- Response{Res: res, Err: rs.Err}
			}
		}

		if rs.Err != "" {
			if nreqs == nil {
				nreqs = c.brqPool.Get().([]clientRequest)
			}
			nreqs = append(nreqs, reqn)
		}
	}

	for i := n + 1; i < len(breq.Reqs); i++ {
		if nreqs == nil {
			nreqs = c.brqPool.Get().([]clientRequest)
		}
		nreqs = append(nreqs, breq.Reqs[i])
	}

	avgWaitDur := (time.Now().UnixNano() - breq.Ts) / int64(len(breq.Reqs))

	c.brqPool.Put(breq.Reqs[:0])
	if len(nreqs) != 0 {
		breq.Reqs = nreqs
		c.batchC <- breq
	}

	c.Lock()
	waitTol := c.avgWaitDur / WaitToleranceRatio

	if c.avgWaitDur == 0 {
		c.avgWaitDur = avgWaitDur
	} else {
		if avgWaitDur > c.avgWaitDur+waitTol {
			c.batchUnits /= 2
			if c.batchUnits == 0 {
				c.batchUnits = 1
			}
		} else if avgWaitDur < c.avgWaitDur-waitTol {
			c.batchUnits++
		}
		c.avgWaitDur = c.avgWaitDur/WeightRatio + avgWaitDur - avgWaitDur/WeightRatio
	}
	c.Unlock()
}

func (c *Client) reqHandler() {
	defer c.wg.Done()

	limitC := make(chan struct{}, MaxConnPerClient)
	for {
		select {
		case <-c.qC:
			goto reqLoopExit
		default:
			if breq := c.buildBatch(); breq != nil {
				limitC <- struct{}{}
				c.wg.Add(1)
				go c.process(breq, limitC)
			}
		}
	}
reqLoopExit:
}

func (c *Client) makeRequest(req clientRequest, respC chan Response) {
	c.reqC <- internalClientRequest{Ts: time.Now().UnixNano(), Req: req, RespC: respC}
}

func (c *Client) ReadAt(k []byte, t int64) <-chan Response {
	respC := make(chan Response, 1)
	c.makeRequest(clientRequest{Typ: ReadType, Req: &ReadRequest{T: t, K: k}}, respC)
	return respC
}

func (c *Client) Read(k []byte) (respC <-chan Response) {
	return c.ReadAt(k, -1)
}

func (c *Client) Write(k []byte, v []byte) <-chan Response {
	respC := make(chan Response, 1)
	c.makeRequest(clientRequest{Typ: WriteType, Req: &WriteRequest{K: k, V: v}}, respC)
	return respC
}

func (c *Client) config(req configRequest) (err error) {
retryLoop:
	resp, err := c.post(configPath, req)
	defer func() {
		if resp != nil {
			resp.Body.Close()
		}
	}()

	if err != nil {
		return
	}

	if resp.StatusCode == http.StatusOK {
		return
	}

	var rb []byte
	if rb, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	switch resp.StatusCode {
	case http.StatusTemporaryRedirect:
		url, _ := resp.Location()
		c.ldrUrl = url.String()
		goto retryLoop

	case http.StatusBadRequest:
		err = fmt.Errorf(ErrReq)

	default:
		err = fmt.Errorf(string(rb))
	}

	return
}

type configRequest struct {
	Typ string       `json:"Type"`
	Req raft.Command `json:"Request"`
}

func (c *Client) Add(srvId string) (err error) {
	return c.config(configRequest{"Add", &raft.AddServerCommand{ServerId: srvId}})
}

func (c *Client) Remove(srvId string) (err error) {
	return c.config(configRequest{"Del", &raft.DelServerCommand{ServerId: srvId}})
}

func (c *Client) Reconfigure(srvs []string) (err error) {
	return c.config(configRequest{"Reconfigure", &raft.ReconfigureCommand{Servers: srvs}})
}
