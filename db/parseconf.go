package db

import (
	"encoding/xml"
	"os"

	"bitbucket.org/jpathy/dmc/ndb"
)

type Config struct {
	Uuid string `xml:"DB>InitFixed>UUID"`

	DBPath string `xml:"DB>InitFixed>DBPath"`

	HashPower uint32 `xml:"DB>InitFixed>CuckooTablePower"`
	Slots     uint32 `xml:"DB>InitFixed>CuckooSlots"`

	MaxHStore uint32 `xml:"DB>Variables>HashStoreCount"`
	Versions  uint32 `xml:"DB>Variables>Versions"`
	Port      uint16 `xml:"DB>Variables>HttpListenPort"`

	ndb.Config
}

var defaultConfig = Config{
	DBPath:    "/tmp/testdb",
	HashPower: 13,
	Slots:     4,
	MaxHStore: 10,
	Versions:  256,
	Port:      2718,
}

func ParseDBConfig(confPath string) (conf *Config, err error) {
	f, err := os.Open(confPath)
	conf = new(Config)
	if err == nil {
		defer f.Close()
		err = xml.NewDecoder(f).Decode(conf)
	}
	if err != nil {
		*conf = defaultConfig
	}
	return
}
