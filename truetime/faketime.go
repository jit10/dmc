package TT

import (
	"math/rand"
	"time"
)

func InitGPS() {
	ε = int64(10 * time.Millisecond)
}

func addUncertainity(t int64) int64 {
	e := rand.Int63n(ε)
	if rand.Intn(2) != 0 {
		e = -e
	}
	return t + e
}

func GPSTime() (t int64) {
	t = time.Now().UnixNano()
	t = addUncertainity(t)
	return
}
