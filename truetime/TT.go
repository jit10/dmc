package TT

import (
	"time"
)

var ε int64

type TIntval struct {
	Et int64
	Lt int64
}

func (ti TIntval) Duration() (d int64) {
	if ti.Lt >= ti.Et {
		d = ti.Lt - ti.Et
	}
	return
}

func (t1 TIntval) Contains(t2 TIntval) bool {
	return t1.Et < t2.Et && t2.Lt < t1.Lt
}

func Now() (t TIntval) {
	abs := GPSTime()
	t = TIntval{abs - ε, abs + ε}
	return
}

func Before(t int64) bool {
	ti := Now()
	return ti.Lt < t
}

func After(t int64) bool {
	ti := Now()
	return ti.Et > t
}

func Until(t int64) {
	ti := Now()
	for ti.Et <= t {
		time.Sleep(time.Duration(t-ti.Et))
		ti = Now()
	}
}

