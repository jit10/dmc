package main

import (
	"fmt"

	"bitbucket.org/jpathy/dmc/ndb"
)

var secret = "8Cxt0qGhcOv0IRyO38Qt"

func main() {
	s, err := ndb.NewServer([]byte(secret))
	if err == nil {
		s.Serve()
	} else {
		fmt.Println(err)
	}
}
