package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/jpathy/dmc/ndb"
)

var secret = "8Cxt0qGhcOv0IRyO38Qt"

func main() {
	conf := &ndb.Config{
		NSHost:   "127.0.0.1",
		NSSecret: secret,
	}
	c := ndb.NewClient(conf)

	fs1 := flag.NewFlagSet("announce", flag.ExitOnError)
	uid := fs1.String("uid", "", "Uid of the client")
	dbport := fs1.Uint("dbport", 0, "Address of the DB Server")
	rport := fs1.Uint("raftport", 0, "Address of the Raft Server")

	fs2 := flag.NewFlagSet("query", flag.ExitOnError)
	quid := fs2.String("uid", "", "Uid of the client")

	args := os.Args[1:]
	if len(args) <= 1 {
		fs2.Parse(args)
		infos, err := c.Query(*quid)
		if err != nil {
			fmt.Println(err)
		}
		for _, info := range infos {
			fmt.Println(info)
		}
	} else {
		fs1.Parse(args)
		if err := c.Announce(*uid, *dbport, *rport); err != nil {
			fmt.Println(err)
		}
	}
}
