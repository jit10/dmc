package main

import (
	"flag"
	"fmt"
	"os"
	"runtime/pprof"

	"bitbucket.org/jpathy/dmc/db"
)

var confPath string
var cpuprofile string

func init() {
	flag.StringVar(&confPath, "config", "", "server configuration file")
	flag.StringVar(&cpuprofile, "cpuprofile", "", "write cpu profile to file")
}

func main() {
	flag.Parse()

	if cpuprofile != "" {
		f, err := os.Create(cpuprofile)
		if err != nil {
			fmt.Println(err)
			return
		}
		pprof.StartCPUProfile(f)
	}

	s, err := db.NewServer(confPath)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(s.Serve())
	fmt.Println(s.Close())

	pprof.StopCPUProfile()
}
