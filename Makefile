GOGOPROTOPATH := $(GOPATH)/src/code.google.com/p/gogoprotobuf
PROTOPATH := db
PROTOFILES := $(wildcard $(PROTOPATH)/*.proto)
PBFILES := $(PROTOFILES:%.proto=%.pb.go)

.PHONY: proto all

all: proto bins
proto:	protoclean protogen
protoclean:
	rm -f $(PBFILES)
protogen:
	protoc --proto_path=$(GOPATH)/src:$(GOGOPROTOPATH)/protobuf:$(PROTOPATH) --gogo_out=$(PROTOPATH) $(PROTOFILES)
	sed -ri ':a; s%(.*)/\*.*\*/%\1%; ta; /\/\*/ !b; N; ba' $(PBFILES)
bins:
	go build ndbserver.go
	go build dbserver.go
