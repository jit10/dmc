package cuckoo

import (
	"fmt"
	"io"
	"math/rand"
	"sync"

	"bitbucket.org/jpathy/dmc/cityhash"
)

const (
	MurMurConst = 0x5bd1e995

	MaxKickCount      = 512
	MaxPower          = 24  // Artifical limit by 32-bit hash and 8-bit tag size (cityhash64 can increase; but unncessary)
	MaxSlotsPerBucket = 255 // Artificial limit for 32-bit index for MemTable
)

var (
	ETableFull = fmt.Errorf("Cuckoo Table is Full")
	EWrite     = fmt.Errorf("CuckooStorage is not writable")
)

type PositionTable interface {
	GetPosAt(idx int64) (pos int64)
	PutPosAt(idx, pos int64) (err error)
}

/* For Read-Only Storage KeyIterator() must iterate in the index order of PositionTable */
type CuckooStorage interface {
	CompareKeyAt(k []byte, pos int64) bool
	KeyIterator() func() (pos int64, k []byte, err error)
	PosTbl() PositionTable
}

type CuckooTable struct {
	sync.RWMutex
	rdonly   bool
	slots    uint32
	hashmask uint32
	path     []uint32
	tag      []byte
	pos      PositionTable
	CuckooStorage
}

func keyHash(key []byte) uint32 {
	return city.Hash32(key)
}

func hashTag(hash uint32) (tag byte) {
	tag = byte(hash >> 24)
	if tag == 0 {
		tag = 0x80
	}
	return
}

func (t *CuckooTable) altBucket(tag uint8, bucket uint32) uint32 {
	return (bucket ^ (uint32(tag) * MurMurConst)) & t.hashmask
}

func (t *CuckooTable) buckets(hash uint32) (uint32, uint32) {
	i1 := hash & t.hashmask
	return i1, t.altBucket(hashTag(hash), i1)
}

func (t *CuckooTable) emptySlot(b uint32) (s uint32) {
	s = t.slots
	idx := b * t.slots
	for i := uint32(0); i < t.slots; i++ {
		if t.IsEmpty(idx) {
			s = i
			break
		}
		idx++
	}
	return
}

func (t *CuckooTable) matchIndex(k []byte, hash, b1, b2 uint32) <-chan uint32 {
	var wg sync.WaitGroup
	index := make(chan uint32, 1)
	defer func() {
		wg.Wait()
		close(index)
	}()

	findmatch := func(idx uint32) {
		if t.tag[idx] == hashTag(hash) && t.CompareKeyAt(k, t.pos.GetPosAt(int64(idx))) {
			select {
			case index <- idx:
			default:
			}
		}
		wg.Done()
	}

	i1, i2 := b1*t.slots, b2*t.slots
	wg.Add(2 * int(t.slots))
	for i := uint32(0); i < t.slots; i++ {
		go findmatch(i1)
		go findmatch(i2)
		i1++
		i2++
	}
	return index
}

func (t *CuckooTable) holeSearch(b uint32) bool {
	t.path = t.path[:0]
	exists := make(map[uint32]bool)
	s := uint32(rand.Intn(int(t.slots)))
	idx := b*t.slots + s
	for i := 0; i < cap(t.path); i++ {
		t.path = append(t.path, idx)
		exists[idx] = true
		b = t.altBucket(t.tag[idx], b)
		if s = t.emptySlot(b); s < t.slots {
			t.path = append(t.path, b*t.slots+s)
			return true
		} else {
			var ok bool
			if idx = b*t.slots + uint32(rand.Intn(int(t.slots))); !exists[idx] {
				continue
			}
			for i := uint32(0); i < t.slots; i++ {
				idx = b*t.slots + i
				if ok = !exists[idx]; ok {
					break
				}
			}
			if !ok {
				break
			}
		}
	}
	return false
}

func (t *CuckooTable) holeFill() (err error) {
	for i := len(t.path) - 1; i > 0; i-- {
		dst := t.path[i]
		src := t.path[i-1]
		t.tag[dst] = t.tag[src]
		if err = t.pos.PutPosAt(int64(dst), t.pos.GetPosAt(int64(src))); err != nil {
			break
		}
	}
	return
}

func (t *CuckooTable) indexOf(k []byte) (index int64) {
	hash := keyHash(k)
	b1, b2 := t.buckets(hash)
	idxch := t.matchIndex(k, hash, b1, b2)
	idx, ok := <-idxch
	if ok {
		index = int64(idx)
	} else {
		index = -1
	}
	return
}

func (t *CuckooTable) initmemTable() (err error) {
	var pos, idx int64
	var k []byte

	iter := t.KeyIterator()
	pos, k, err = iter()
	for err == nil {
		if !t.rdonly {
			if k == nil {
				err = fmt.Errorf("nil key encountered while iterating")
				break
			}
			if idx = t.indexOf(k); idx == -1 {
				err = t.PositionAt(k, pos)
			} else {
				t.tag[idx] = hashTag(keyHash(k))
				err = t.pos.PutPosAt(idx, pos)
			}
			if err != nil {
				break
			}
		} else {
			if k != nil {
				t.tag[idx] = hashTag(keyHash(k))
			}
			idx++
		}
		pos, k, err = iter()
	}
	if err == io.EOF {
		err = nil
	}
	return
}

/* Exported Functions */

func NewCuckooTable(power, slots uint32, s CuckooStorage, rdonly bool) (t *CuckooTable, err error) {
	hashmask := uint32((1 << power) - 1)
	if slots > MaxSlotsPerBucket {
		return nil, fmt.Errorf("No of slots per bucket(have %d) can't be greater than %d", slots, MaxSlotsPerBucket)
	} else if power > MaxPower {
		return nil, fmt.Errorf("No of buckets in table(have %d) can't be greater than %d", power, MaxPower)
	}
	t = &CuckooTable{
		rdonly:        rdonly,
		slots:         slots,
		hashmask:      hashmask,
		path:          make([]uint32, MaxKickCount),
		tag:           make([]byte, slots*(1<<power)),
		pos:           s.PosTbl(),
		CuckooStorage: s,
	}
	err = t.initmemTable()
	return
}

func (t *CuckooTable) IsEmpty(index uint32) bool {
	return t.tag[index] == 0
}

func (t *CuckooTable) PositionOf(k []byte) (pos int64) {
	if !t.rdonly {
		t.RLock()
	}

	idx := t.indexOf(k)
	if idx == -1 {
		pos = -1
	} else {
		pos = t.pos.GetPosAt(idx)
	}

	if !t.rdonly {
		t.RUnlock()
	}
	return
}

func (t *CuckooTable) PositionAt(k []byte, pos int64) (err error) {
	if t.rdonly {
		return EWrite
	}

	t.Lock()

	hash := keyHash(k)
	b1, b2 := t.buckets(hash)

	var ok bool
	var index int64
	if s := t.emptySlot(b1); s < t.slots {
		index, ok = int64(b1*t.slots+s), true
	} else if s = t.emptySlot(b2); s < t.slots {
		index, ok = int64(b2*t.slots+s), true
	} else {
		for i := uint32(0); i < t.slots; i++ {
			if ok = t.holeSearch(b1); !ok {
				ok = t.holeSearch(b2)
			}
			if ok {
				if err = t.holeFill(); err != nil {
					ok = false
				}
				break
			}
		}
		if ok {
			index = int64(t.path[0])
		}
	}
	if ok {
		t.tag[index] = hashTag(hash)
		err = t.pos.PutPosAt(int64(index), pos)
	} else if err == nil {
		err = ETableFull
	}

	t.Unlock()
	return
}
