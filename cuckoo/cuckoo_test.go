package cuckoo

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"io"
	"sync/atomic"
	"testing"
)

const (
	Power         = 13
	Slots         = 4
	EntrySize     = (1 << 5)
	MaxEntryCount = (Slots * (1 << Power) * 93) / 100 // Atleast 93% without being full
)

type offsets []uint32

/* Satisfy cuckoo.PositionTable interface */

func (offtbl offsets) GetPosAt(idx int64) (pos int64) {
	return int64(offtbl[idx])
}

func (offtbl offsets) PutPosAt(idx, pos int64) error {
	offtbl[idx] = uint32(pos)
	return nil
}

/* ... */

/*
Entries are keyed by sha1 value; Entry := len[2] data[n]
*/
type memKVS struct {
	offtbl offsets
	pos    uint32
	log    []byte
	t      *CuckooTable
}

/* Satisfy cuckoo.CuckooStorage interfeace */

func (s *memKVS) CompareKeyAt(k []byte, pos int64) bool {
	l := int64(binary.LittleEndian.Uint16(s.log[pos:]))
	pos += 2
	h := sha1.Sum(s.log[pos : pos+l])
	return bytes.Equal(k, h[:])
}

func (s *memKVS) KeyIterator() func() (pos int64, k []byte, err error) {
	ipos := int64(0)
	return func() (pos int64, k []byte, err error) {
		if ipos >= int64(s.pos) {
			return 0, nil, io.EOF
		}
		l := int64(binary.LittleEndian.Uint16(s.log[ipos:]))
		h := sha1.Sum(s.log[ipos+2 : ipos+2+l])
		k = h[:]
		pos = ipos
		ipos += l + 2
		return
	}
}

func (s *memKVS) PosTbl() PositionTable {
	return s.offtbl
}

/* ... */

func (s *memKVS) Put(k, v []byte) error {
	elen := uint32(len(v) + 2)
	npos := atomic.AddUint32(&s.pos, elen)
	if int(npos) > cap(s.log) {
		return fmt.Errorf("store Full")
	}
	opos := npos - elen
	binary.LittleEndian.PutUint16(s.log, uint16(elen-2))
	copy(s.log[opos+2:], v)
	return s.t.PositionAt(k, int64(opos))
}

func (s *memKVS) GetAndVerify(k []byte) bool {
	pos := s.t.PositionOf(k)
	if pos == -1 {
		return false
	}
	l := int64(binary.LittleEndian.Uint16(s.log[pos:]))
	pos += 2
	h := sha1.Sum(s.log[pos : pos+l])
	return bytes.Equal(k, h[:])
}

func randData() (d []byte) {
	d = make([]byte, EntrySize)
	io.ReadFull(rand.Reader, d)
	return
}

func TestCuckooTable(t *testing.T) {
	s := &memKVS{
		offtbl: make([]uint32, Slots*(1<<Power)),
		pos:    0,
		log:    make([]byte, (EntrySize+2)*MaxEntryCount),
	}
	ct, err := NewCuckooTable(Power, Slots, s, false)
	if err != nil {
		t.Error(err)
		return
	}
	s.t = ct

	keys := make([]byte, sha1.Size*MaxEntryCount)

	var ecount int
	for ecount := 0; ecount < MaxEntryCount; ecount++ {
		v := randData()
		k := sha1.Sum(v)
		if err := s.Put(k[:], v); err != nil {
			t.Error(err)
			break
		}
		copy(keys[ecount*sha1.Size:], k[:])
	}

	ecount--
	/* Read and Verify */
	for i := 0; i < ecount; i++ {
		o := i * sha1.Size
		if !s.GetAndVerify(keys[o : o+sha1.Size]) {
			t.Errorf("Failed at %d\n", i)
			break
		}
	}
}
