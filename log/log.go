package log

import (
	"errors"
	"io"
	"os"
)

const (
	DBLogName = "db.log"
)

var (
	NilFName = errors.New("Log Name cannot be empty")
)

type Header interface {
	Marshal(wire io.Writer) error
	Unmarshal(wire io.Reader) error
	Size() int
}

type Log interface {
	ReadHeader(hdr Header, eoff int64) error
	ReadData(d []byte, doff int64) error

	Dup(w io.Writer) error

	Size() int64
	Sync() error
	Close() error
}

func open(path string, flag int) (f *os.File, err error) {
	if path == "" {
		err = NilFName
		return
	}

	var i int
	for i = len(path) - 1; i >= 0 && path[i] != '/'; i-- {
	}

	err = os.MkdirAll(path[:i], 0755)
	if err != nil && !os.IsExist(err) {
		return
	}
	f, err = os.OpenFile(path, flag, 0600)
	return
}

func OpenMutable(path string) (l Mutable, err error) {
	f, err := open(path, os.O_CREATE|os.O_RDWR)
	if err != nil {
		return
	}
	mf, err := mmap(f)
	if err == nil {
		l = Mutable{mf}
	}
	return
}

func OpenImmutable(path string) (l Immutable, err error) {
	f, err := open(path, os.O_RDONLY)
	l = Immutable{f}
	return
}
