package log

import (
	"errors"
	"io"
	"os"

	. "bitbucket.org/jpathy/dmc/util"
)

var (
	WRErr    = errors.New("Immutable log has no write capability")
	EImmSeek = errors.New("Immutable log doesn't allow seek operation")
)

type Immutable struct {
	*os.File
}

func (l Immutable) ReadHeader(hdr Header, eoff int64) error {
	err := hdr.Unmarshal(SeekReader{l, eoff})
	return err
}

func (l Immutable) ReadData(d []byte, doff int64) error {
	_, err := l.ReadAt(d, doff)
	return err
}

func (l Immutable) Write(hdr Header, data ...[]byte) (pos int64, err error) {
	return 0, WRErr
}

func (l Immutable) Size() int64 {
	st, err := l.Stat()
	if err == nil {
		return st.Size()
	} else {
		return 0
	}
}

func (l Immutable) Dup(w io.Writer) error {
	_, err := io.Copy(w, l.File)
	l.File.Seek(0, os.SEEK_SET)
	return err
}

/* Sync() and Close() are implemented by Embedded *os.File */
