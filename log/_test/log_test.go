package log

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"runtime"
	"testing"
	"time"

	. "bitbucket.org/jpathy/dmc/log"
)

const (
	tmppath = "/tmp"
	mprefix = "MuTaBlElOg"
	logName = "log"

	maxKeySize = 1 << 12
	maxValSize = 1 << 16
	entryCount = 1023
)

var l Mutable
var keylist [][]byte
var vallist [][]byte
var offlist = make([]uint32, entryCount)

func randData(l int) (d []byte) {
	d = make([]byte, rand.Intn(l))
	for i := 0; i < len(d); i++ {
		d[i] = byte(rand.Int63() & 0xff)
	}
	return
}

func writer(index <-chan int, echan chan<- error) {
	for i := range index {
		go func(i int) {
			key := keylist[i]
			val := vallist[i]
			n, err := l.Write(&tstHdr{
					uint32(len(key)),
					uint32(len(val)),
				},
				key, val)
			offlist[i] = uint32(n)
			echan <- err
		}(i)
	}
}

func reader(index <-chan int, echan chan<- error) {
	for i := range index {
		var hdr tstHdr
		if err := l.ReadHeader(&hdr, int64(offlist[i])); err != nil {
			echan <- fmt.Errorf("tstHdr Read failed at offset %d", i)
			continue
		}
		if hdr.klen != uint32(len(keylist[i])) {
			echan <- fmt.Errorf("Key length mismatch(read %d, have %d) at offset %d", hdr.klen, len(keylist[i]), i)
			continue
		}
		if hdr.vlen != uint32(len(vallist[i])) {
			echan <- fmt.Errorf("Value length mismatch(read %d, have %d) at offset %d", hdr.klen, len(keylist[i]), i)
			continue
		}
		key := make([]byte, len(keylist[i]))
		if err := l.ReadData(key, int64(offlist[i]+uint32(hdr.Size()))); err != nil {
			echan <- err
			continue
		}
		if bytes.Compare(key, keylist[i]) != 0 {
			echan <- fmt.Errorf("Key Comparison failed")
			continue
		}
		val := make([]byte, len(vallist[i]))
		if err := l.ReadData(val, int64(offlist[i]+uint32(hdr.Size()+len(key)))); err != nil {
			echan <- err
			continue
		}
		if bytes.Compare(val, vallist[i]) != 0 {
			echan <- fmt.Errorf("Value Comparison failed")
			continue
		}
		echan <- nil
	}
}

func TestMutableLog(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	tmpdir, err := ioutil.TempDir(tmppath, mprefix)
	if err != nil {
		panic(err)
	}

	l, err = OpenMutable(tmpdir + string(os.PathSeparator) + logName)
	if err != nil {
		panic(err)
	}

	/* Generate */
	for i := 0; i < entryCount; i++ {
		key := randData(maxKeySize)
		val := randData(maxValSize)
		keylist = append(keylist, key)
		vallist = append(vallist, val)
	}
	echan := make(chan error, entryCount)

	/* Write */
	index := make(chan int, entryCount)
	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go writer(index, echan)
	}
	for i := 0; i < entryCount; i++ {
		index <- i
	}
	for i := 0; i < entryCount; i++ {
		if err := <-echan; err != nil {
			t.Error(err)
		}
	}
	close(index)

	/* Read and verify */
	index = make(chan int, entryCount)
	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go reader(index, echan)
	}
	for i := 0; i < entryCount; i++ {
		index <- i
	}
	for i := 0; i < entryCount; i++ {
		if err := <-echan; err != nil {
			t.Error(err)
		}
	}
	close(index)

	close(echan)

	/* Sync and close */
	if err := l.Close(); err != nil {
		t.Error(err)
	}
	os.RemoveAll(tmpdir)
}

func BenchmarkMutableWrite(b *testing.B) {
	rand.Seed(time.Now().UnixNano())
	tmpdir, err := ioutil.TempDir(tmppath, mprefix)
	if err != nil {
		panic(err)
	}

	/* Generate */
	for i := 0; i < entryCount; i++ {
		key := randData(maxKeySize)
		val := randData(maxValSize)
		keylist = append(keylist, key)
		vallist = append(vallist, val)
	}
	echan := make(chan error, entryCount)

	for i := 0; i < b.N; i++ {
		l, err = OpenMutable(tmpdir + string(os.PathSeparator) + logName)
		if err != nil {
			panic(err)
		}

		/* Write */
		index := make(chan int, entryCount)
		for i := 0; i < runtime.GOMAXPROCS(0); i++ {
			go writer(index, echan)
		}
		for i := 0; i < entryCount; i++ {
			index <- i
		}
		for i := 0; i < entryCount; i++ {
			if err := <-echan; err != nil {
				b.Error(err)
			}
		}
		close(index)

		/* Sync and close */
		if err := l.Close(); err != nil {
			b.Error(err)
		}
		if err := os.Remove(tmpdir + string(os.PathSeparator) + logName); err != nil {
			b.Error(err)
			break
		}
	}
	close(echan)
	os.RemoveAll(tmpdir)
}

func BenchmarkMutableRead(b *testing.B) {
	rand.Seed(time.Now().UnixNano())
	tmpdir, err := ioutil.TempDir(tmppath, mprefix)
	if err != nil {
		panic(err)
	}

	/* Generate */
	for i := 0; i < entryCount; i++ {
		key := randData(maxKeySize)
		val := randData(maxValSize)
		keylist = append(keylist, key)
		vallist = append(vallist, val)
	}
	echan := make(chan error, entryCount)

	l, err = OpenMutable(tmpdir + string(os.PathSeparator) + logName)
	if err != nil {
		panic(err)
	}

	/* Write */
	index := make(chan int, entryCount)
	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go writer(index, echan)
	}
	for i := 0; i < entryCount; i++ {
		index <- i
	}
	for i := 0; i < entryCount; i++ {
		if err := <-echan; err != nil {
			b.Error(err)
		}
	}
	close(index)

	for i := 0; i < b.N; i++ {
		/* Read and verify */
		index = make(chan int, entryCount)
		for i := 0; i < runtime.GOMAXPROCS(0); i++ {
			go reader(index, echan)
		}
		for i := 0; i < entryCount; i++ {
			index <- i
		}
		for i := 0; i < entryCount; i++ {
			if err := <-echan; err != nil {
				b.Error(err)
			}
		}
		close(index)
	}
	close(echan)

	/* Sync and close */
	if err := l.Close(); err != nil {
		b.Error(err)
	}
	os.RemoveAll(tmpdir)
}
