package log

import (
	"errors"
	"io"
	"os"
	"sync"
	"sync/atomic"
	"syscall"

	. "bitbucket.org/jpathy/dmc/util"
)

const (
	MB         = 1024 * 1024
	MaxLogSize = 64 * MB
	GrowSize   = 2 * MB
)

var (
	ELogFull = errors.New("Log is Full")
	ESeek    = errors.New("Forward seeking is not allowed")
)

type mmapFile struct {
	sync.Mutex
	wpos uint32
	end  uint32
	incr uint32
	buf  ByteBuffer
	f    *os.File
}

type Mutable struct {
	*mmapFile
}

func (l Mutable) ReadHeader(hdr Header, eoff int64) error {
	if int(eoff) > cap(l.buf) {
		return io.EOF
	}
	err := hdr.Unmarshal(l.buf[eoff:])
	return err
}

func (l Mutable) ReadData(d []byte, doff int64) error {
	if int(doff) > cap(l.buf) {
		return io.EOF
	}
	_, err := l.buf[doff:].Read(d)
	return err
}

func (l Mutable) WriteAt(b []byte, pos int64) (n int, err error) {
	n, err = l.buf[pos:].Write(b)
	return
}

func (l Mutable) Write(hdr Header, data ...[]byte) (pos int64, err error) {
	hdrsz := uint32(hdr.Size())
	dlen := 0
	for _, d := range data {
		dlen += len(d)
	}
	elen := hdrsz + uint32(dlen)
	npos := atomic.AddUint32(&l.wpos, elen)
	if npos > uint32(cap(l.buf)) {
		atomic.AddUint32(&l.wpos, ^(elen - 1))
		return 0, ELogFull
	}
	if npos > atomic.LoadUint32(&l.end) {
		l.Lock()
		if npos > l.end {
			if l.incr < elen {
				l.incr = elen
			}
			end := l.end + l.incr
			if npos > end || end > uint32(cap(l.buf)) {
				end = npos
			}
			if err = l.f.Truncate(int64(end)); err != nil {
				l.Unlock()
				return 0, err
			}
			l.end = end
		}
		l.Unlock()
	}
	hdr.Marshal(l.buf[npos-elen:])

	dlen = 0
	for _, d := range data {
		copy(l.buf[npos-elen+hdrsz+uint32(dlen):], d)
		dlen += len(d)
	}

	return int64(npos - elen), nil
}

func (l Mutable) Size() int64 {
	size := atomic.LoadUint32(&l.wpos)
	return int64(size)
}

func (l Mutable) Sync() error {
	err := msync(l.buf, atomic.LoadUint32(&l.wpos))
	return err
}

func (l Mutable) Dup(w io.Writer) error {
	_, err := w.Write(l.buf[:atomic.LoadUint32(&l.wpos)])
	return err
}

func (l Mutable) Close() error {
	err := syscall.Munmap(l.buf)
	if err != nil {
		return err
	}
	if err = l.f.Truncate(int64(l.wpos)); err == nil {
		err = l.f.Close()
	}
	return err
}
