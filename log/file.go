package log

import (
	"fmt"
	"os"
	"reflect"
	"syscall"
	"unsafe"

	. "bitbucket.org/jpathy/dmc/util"
)

func mmap(f *os.File) (mf *mmapFile, err error) {
	st, err := f.Stat()
	if err != nil {
		return
	}
	n := int64(os.Getpagesize())
	size := (st.Size() + n) &^ (n - 1)
	if size > MaxLogSize {
		return nil, fmt.Errorf("File %s is larger than max size %d", st.Name(), MaxLogSize)
	} else {
		size = MaxLogSize
	}
	buf, err := MmapFile(f, MMAP_RD|MMAP_WR, 0, size)
	if err != nil {
		return nil, err
	}
	mf = &mmapFile{
		wpos: uint32(st.Size()),
		end:  uint32(st.Size()),
		incr: GrowSize,
		buf:  buf,
		f:    f,
	}
	return mf, nil
}

func msync(b []byte, size uint32) (err error) {
	shdr := *(*reflect.SliceHeader)(unsafe.Pointer(&b))
	_, _, errno := syscall.Syscall(syscall.SYS_MSYNC, shdr.Data, uintptr(size), syscall.MS_SYNC)
	if errno != 0 {
		err = errno
	}
	return
}
